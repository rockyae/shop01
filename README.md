# 系统设计

## 1.1 系统体系架构

单体架构商城开发是一个基于springboot和mybatis-plus框架进行开发的web后端开发，通过response接收前端传过来的数据，后端再通过json格式返回数据到前端。

## 1.2 功能模块设计

单体架构商城开发主要分为以下几个模块：

#### 1)用户管理模块

用户管理模块负责管理用户的登录、注册以及用户信息修改等管理功能

#### 2)商品管理模块

商品管理模块负责商品列表的展示以及管理员后台增加商品以及管理商品的上架或者下架操作

#### 3)地址管理模块

地址管理模块负责用户地址的新增、修改、删除以及修改默认地址等功能

#### 4)订单管理模块

订单管理模块负责管理订单新增、定时清理已过期的未付款的订单、订单付款、收货、评价等各种功能

## 1.3类的设计

基于springboot的项目沿用了MVC的架构，项目骨架如下图所示：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1104/220109_50ca3e01_7633691.png "屏幕截图.png")

各包的解释说明：
Config：负责数据库连接、拦截器注册、分页控件、redis设置
Controller：负责接收前端传过来的数据以及数据处理，并返回json格式的数据给前端
Dto：定义返回数据的格式
Entity：实体类以及接受数据的request和返回数据的response类
Enums：抛出异常的异常枚举以及订单状态枚举类
Exception：异常模块，负责抛出各种自定义异常
Interceptor：负责用户访问没有权限页面的拦截处理
Job：设定将未按时支付的订单修改为已失效订单的定时处理任务
Mapper：mapper.xml的接口类
Service：实现各种业务的业务类
Utils：各种工具类

## 1.4 数据库设计

Address表：

Category表：

Order_evaluate表：

Order_product表：

Product表：

User表：

User_order表：

# 如何运行

1.git clone 项目到本地。

2.前端环境：Hbuilder;
  后端环境：IDEA;
  数据库：mysql,redis;
项目运行步骤：用Hbuilder运行前端项目item-shop，idea运行shop项目;将shop.sql脚本导入到本机mysql的shop数据库。启动本机redis服务器；

3.更改application-test的配置的数据库用户名密码为你的本地配置；

4.运行成功后可以使用测试账户进行登录。（用户名：user 密码：123456）

5.本项目没有考虑高并发情况。



