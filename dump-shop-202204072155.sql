-- MySQL dump 10.13  Distrib 5.6.11, for Win64 (x86_64)
--
-- Host: localhost    Database: shop
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `nickname` varchar(255) DEFAULT NULL COMMENT '名字',
  `telephone` varchar(255) DEFAULT NULL COMMENT '电话',
  `address_desc` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `postal_code` varchar(255) DEFAULT NULL COMMENT '邮政编码',
  `is_default` varchar(255) NOT NULL COMMENT '是否为默认地址0:默认是，1不是',
  `address_num` int(11) DEFAULT NULL COMMENT '地址使用计数器',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_id_2` (`user_id`),
  CONSTRAINT `FK_user_id_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地址表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (2945201482211592,1372813512586526721,'孙军','18934445566','深圳市','23','1',NULL,'2022-03-27 12:50:40','2022-03-27 16:22:26'),(2945313650106632,1372813512586526721,'123','17879504656','1231','2','1',NULL,'2022-03-27 16:38:52','2022-03-27 16:38:52'),(1375282968121217026,1372720720463921154,'423423324','2344223443','234324','243342','0',NULL,'2021-03-26 03:06:26','2021-03-26 03:06:26'),(1375635327678550016,1372813512586526721,'xcj','15079404567','12312','123','0',NULL,'2021-03-27 10:26:35','2022-03-27 16:22:26'),(1401466359015149568,1372813568987332610,'xcj','1555','xxxx','xx','0',NULL,'2021-06-06 17:09:53','2021-06-06 17:09:52');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `category_name` varchar(255) DEFAULT NULL COMMENT '类别名称',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1373829361967529986,'手机','2021-03-22 10:50:19','2021-03-22 10:50:19'),(1373891085827502081,'电脑','2021-03-22 06:55:35','2021-03-22 06:55:35'),(1373891127846039554,'食品','2021-03-22 06:55:45','2021-03-22 06:55:45'),(1373891219625799682,'电脑配件','2021-03-22 06:56:07','2021-03-22 06:56:07');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_evaluate`
--

DROP TABLE IF EXISTS `order_evaluate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_evaluate` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `evaluate_desc` varchar(255) DEFAULT NULL COMMENT '评价的详情',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单id',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Reference_8` (`order_id`),
  CONSTRAINT `FK_Reference_8` FOREIGN KEY (`order_id`) REFERENCES `user_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_evaluate`
--

LOCK TABLES `order_evaluate` WRITE;
/*!40000 ALTER TABLE `order_evaluate` DISABLE KEYS */;
INSERT INTO `order_evaluate` VALUES (1508010825329324033,'123',1502914877323350017,'2022-03-27 17:19:33','2022-03-27 17:19:33');
/*!40000 ALTER TABLE `order_evaluate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_product` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单id',
  `product_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `product_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `product_title` varchar(255) DEFAULT NULL COMMENT '商品标题',
  `product_desc` varchar(255) DEFAULT NULL COMMENT '商品描述',
  `product_price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `product_img` varchar(255) DEFAULT NULL COMMENT '商品图片',
  `product_num` int(11) DEFAULT NULL COMMENT '商品数量',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_userOrder_id` (`order_id`),
  KEY `FK_product_id` (`product_id`),
  CONSTRAINT `FK_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_userOrder_id` FOREIGN KEY (`order_id`) REFERENCES `user_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_product`
--

LOCK TABLES `order_product` WRITE;
/*!40000 ALTER TABLE `order_product` DISABLE KEYS */;
INSERT INTO `order_product` VALUES (2945346646753544,2945346646753545,1373892810504335362,'英特尔（Intel）i5-10600KF','英特尔（Intel）i5-10600KF 6核12线程 盒装CPU处理器','【游戏5GHz“芯”十代】LGA1200芯片接口,六核十二线程,睿频可至4.8GHz!推荐联合购买Z490主板享受超频快感!',1299.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d47152e446674e6da64c77dda1f7a72d',1,'2022-03-27 17:46:00','2022-03-27 17:46:00'),(1396398960310095872,1396398960310095873,1373892810504335362,'英特尔（Intel）i5-10600KF','英特尔（Intel）i5-10600KF 6核12线程 盒装CPU处理器','【游戏5GHz“芯”十代】LGA1200芯片接口,六核十二线程,睿频可至4.8GHz!推荐联合购买Z490主板享受超频快感!',1299.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d47152e446674e6da64c77dda1f7a72d',1,'2021-05-23 17:33:51','2021-05-23 17:33:51'),(1396400230307598336,1396400230307598337,1373892810504335362,'英特尔（Intel）i5-10600KF','英特尔（Intel）i5-10600KF 6核12线程 盒装CPU处理器','【游戏5GHz“芯”十代】LGA1200芯片接口,六核十二线程,睿频可至4.8GHz!推荐联合购买Z490主板享受超频快感!',1299.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d47152e446674e6da64c77dda1f7a72d',2,'2021-05-23 17:38:53','2021-05-23 17:38:53'),(1396639308856823808,1396639308856823809,1373892810504335362,'英特尔（Intel）i5-10600KF','英特尔（Intel）i5-10600KF 6核12线程 盒装CPU处理器','【游戏5GHz“芯”十代】LGA1200芯片接口,六核十二线程,睿频可至4.8GHz!推荐联合购买Z490主板享受超频快感!',1299.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d47152e446674e6da64c77dda1f7a72d',1,'2021-05-24 09:28:54','2021-05-24 09:28:54'),(1396645008664301568,1396645008664301569,1373889493661315073,'三星 Galaxy S21','三星 Galaxy S21 5G（SM-G9910）双模5G 骁龙888 超高清专业摄像 120Hz护目屏 游戏手机 8G+128G 墨影灰','限时优惠50元，下单再减250元，到手价4699元，再送500元E卡，实际优惠800元！更有12期白条免息及五折回购服务！】',4949.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d3171655abe34147886ed0909ed75e46',1,'2021-05-24 09:51:33','2021-05-24 09:51:33'),(1396645585733423104,1396645585733423105,1373889493661315073,'三星 Galaxy S21','三星 Galaxy S21 5G（SM-G9910）双模5G 骁龙888 超高清专业摄像 120Hz护目屏 游戏手机 8G+128G 墨影灰','限时优惠50元，下单再减250元，到手价4699元，再送500元E卡，实际优惠800元！更有12期白条免息及五折回购服务！】',4949.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d3171655abe34147886ed0909ed75e46',1,'2021-05-24 09:53:51','2021-05-24 09:53:51'),(1396646991974502400,1396646991974502401,1373889493661315073,'三星 Galaxy S21','三星 Galaxy S21 5G（SM-G9910）双模5G 骁龙888 超高清专业摄像 120Hz护目屏 游戏手机 8G+128G 墨影灰','限时优惠50元，下单再减250元，到手价4699元，再送500元E卡，实际优惠800元！更有12期白条免息及五折回购服务！】',4949.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d3171655abe34147886ed0909ed75e46',1,'2021-05-24 09:59:26','2021-05-24 09:59:26'),(1396652103400820736,1396652103400820737,1373892810504335362,'英特尔（Intel）i5-10600KF','英特尔（Intel）i5-10600KF 6核12线程 盒装CPU处理器','【游戏5GHz“芯”十代】LGA1200芯片接口,六核十二线程,睿频可至4.8GHz!推荐联合购买Z490主板享受超频快感!',1299.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d47152e446674e6da64c77dda1f7a72d',1,'2021-05-24 10:19:45','2021-05-24 10:19:45'),(1400798883180843008,1400798883180843009,1373892810504335362,'英特尔（Intel）i5-10600KF','英特尔（Intel）i5-10600KF 6核12线程 盒装CPU处理器','【游戏5GHz“芯”十代】LGA1200芯片接口,六核十二线程,睿频可至4.8GHz!推荐联合购买Z490主板享受超频快感!',1299.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d47152e446674e6da64c77dda1f7a72d',1,'2021-06-04 20:57:34','2021-06-04 20:57:34'),(1401465968223457280,1401465968223457281,1373892810504335362,'英特尔（Intel）i5-10600KF','英特尔（Intel）i5-10600KF 6核12线程 盒装CPU处理器','【游戏5GHz“芯”十代】LGA1200芯片接口,六核十二线程,睿频可至4.8GHz!推荐联合购买Z490主板享受超频快感!',1299.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d47152e446674e6da64c77dda1f7a72d',1,'2021-06-06 17:08:19','2021-06-06 17:08:19'),(1401466149245423616,1401466149245423617,1373889493661315073,'三星 Galaxy S21','三星 Galaxy S21 5G（SM-G9910）双模5G 骁龙888 超高清专业摄像 120Hz护目屏 游戏手机 8G+128G 墨影灰','限时优惠50元，下单再减250元，到手价4699元，再送500元E卡，实际优惠800元！更有12期白条免息及五折回购服务！】',4949.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d3171655abe34147886ed0909ed75e46',1,'2021-06-06 17:09:03','2021-06-06 17:09:03'),(1435889790590521344,1435889790598909953,1373889493661315073,'三星 Galaxy S21','三星 Galaxy S21 5G（SM-G9910）双模5G 骁龙888 超高清专业摄像 120Hz护目屏 游戏手机 8G+128G 墨影灰','限时优惠50元，下单再减250元，到手价4699元，再送500元E卡，实际优惠800元！更有12期白条免息及五折回购服务！】',4949.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d3171655abe34147886ed0909ed75e46',1,'2021-09-09 16:56:18','2021-09-09 16:56:18'),(1435889790598909952,1435889790598909953,1373891552510930946,'戴尔DELL成就3400','戴尔DELL成就3400 14英寸轻薄性能商务全面屏笔记本电脑(11代i5-1135G7 16G 512G)一年上门+7x24远程服务','11代轻薄商务学生笔记本,高性能Xe显卡,独显级性能,支持双硬盘位扩展,丰富接口,长效续航,更多产品',4199.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/63f02d3394334695872481054f42a93f',1,'2021-09-09 16:56:18','2021-09-09 16:56:18'),(1435890621087879168,1435890621087879169,1373892810504335362,'英特尔（Intel）i5-10600KF','英特尔（Intel）i5-10600KF 6核12线程 盒装CPU处理器','【游戏5GHz“芯”十代】LGA1200芯片接口,六核十二线程,睿频可至4.8GHz!推荐联合购买Z490主板享受超频快感!',1299.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d47152e446674e6da64c77dda1f7a72d',1,'2021-09-09 16:59:36','2021-09-09 16:59:36'),(1445209782989492224,1445209782989492225,1373889863699591170,'小米11','小米11 5G游戏手机【10重豪礼】 8G+128G 黑色 55W充电器套装','【骁龙888现货当天发】赠55W充电器套装+无线充+蓝牙耳机+90天碎屏保+膜+晒单返20+15天价保+运费险+全国联保+10京豆！',4199.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/37b300198eab44c1a1175c3d5b794ad2',1,'2021-10-05 10:10:38','2021-10-05 10:10:38'),(1445210649092296704,1445210649092296705,1373889493661315073,'三星 Galaxy S21','三星 Galaxy S21 5G（SM-G9910）双模5G 骁龙888 超高清专业摄像 120Hz护目屏 游戏手机 8G+128G 墨影灰','限时优惠50元，下单再减250元，到手价4699元，再送500元E卡，实际优惠800元！更有12期白条免息及五折回购服务！】',4949.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d3171655abe34147886ed0909ed75e46',1,'2021-10-05 10:14:04','2021-10-05 10:14:04'),(1445218348194467840,1445218348194467841,1373889863699591170,'小米11','小米11 5G游戏手机【10重豪礼】 8G+128G 黑色 55W充电器套装','【骁龙888现货当天发】赠55W充电器套装+无线充+蓝牙耳机+90天碎屏保+膜+晒单返20+15天价保+运费险+全国联保+10京豆！',4199.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/37b300198eab44c1a1175c3d5b794ad2',1,'2021-10-05 10:44:40','2021-10-05 10:44:40'),(1445248553420197888,1445248553420197889,1373889863699591170,'小米11','小米11 5G游戏手机【10重豪礼】 8G+128G 黑色 55W充电器套装','【骁龙888现货当天发】赠55W充电器套装+无线充+蓝牙耳机+90天碎屏保+膜+晒单返20+15天价保+运费险+全国联保+10京豆！',4199.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/37b300198eab44c1a1175c3d5b794ad2',1,'2021-10-05 12:44:41','2021-10-05 12:44:41'),(1502914877323350016,1502914877323350017,1373889863699591170,'小米11','小米11 5G游戏手机【10重豪礼】 8G+128G 黑色 55W充电器套装','【骁龙888现货当天发】赠55W充电器套装+无线充+蓝牙耳机+90天碎屏保+膜+晒单返20+15天价保+运费险+全国联保+10京豆！',4199.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/37b300198eab44c1a1175c3d5b794ad2',1,'2022-03-13 15:50:04','2022-03-13 15:50:04');
/*!40000 ALTER TABLE `order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` bigint(20) NOT NULL COMMENT 'id主键',
  `cate_id` bigint(20) NOT NULL COMMENT '分类id',
  `product_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `product_title` varchar(255) DEFAULT NULL COMMENT '商品标题',
  `product_desc` varchar(255) DEFAULT NULL COMMENT '商品描述',
  `product_price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `product_img` varchar(255) DEFAULT NULL COMMENT '商品图片',
  `product_status` int(11) DEFAULT '0' COMMENT '商品状态：0-上架，1-下架',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `FK_category_id` (`cate_id`),
  CONSTRAINT `FK_category_id` FOREIGN KEY (`cate_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1373889493661315073,1373829361967529986,'三星 Galaxy S21','三星 Galaxy S21 5G（SM-G9910）双模5G 骁龙888 超高清专业摄像 120Hz护目屏 游戏手机 8G+128G 墨影灰','限时优惠50元，下单再减250元，到手价4699元，再送500元E卡，实际优惠800元！更有12期白条免息及五折回购服务！】',4949.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d3171655abe34147886ed0909ed75e46',1,'2021-03-22 06:49:16','2021-03-22 06:49:16'),(1373889863699591170,1373829361967529986,'小米11','小米11 5G游戏手机【10重豪礼】 8G+128G 黑色 55W充电器套装','【骁龙888现货当天发】赠55W充电器套装+无线充+蓝牙耳机+90天碎屏保+膜+晒单返20+15天价保+运费险+全国联保+10京豆！',4199.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/37b300198eab44c1a1175c3d5b794ad2',0,'2021-03-22 06:50:44','2021-03-22 06:50:44'),(1373891552510930946,1373891085827502081,'戴尔DELL成就3400','戴尔DELL成就3400 14英寸轻薄性能商务全面屏笔记本电脑(11代i5-1135G7 16G 512G)一年上门+7x24远程服务','11代轻薄商务学生笔记本,高性能Xe显卡,独显级性能,支持双硬盘位扩展,丰富接口,长效续航,更多产品',4199.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/63f02d3394334695872481054f42a93f',0,'2021-03-22 06:57:26','2021-03-22 06:57:26'),(1373891858078560258,1373891085827502081,'联想(Lenovo)小新Air15','联想(Lenovo)小新Air15 2021锐龙版全面屏办公轻薄笔记本电脑(8核R7-4800U 16G 512G 100%sRGB 高色域)深空灰','爆款，大屏硬实力】学生网课办公娱乐,八核芯满血释放，全金属高色域，数字小键盘，70Wh大电池,快充',5199.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/6326e7d4658743688220d412b89d49dd',0,'2021-03-22 06:58:39','2021-03-22 06:58:39'),(1373892411919626241,1373891085827502081,'RedmiBook Pro 15','RedmiBook Pro 15 轻薄本(i5-11300H 16G 512G PCIE MX450 3.2K 90Hz超视网膜全面屏) 红米小米笔记本电脑','3.2K|90Hz超视网膜全面屏!英特尔全新标压H35处理器! 微米级一体精雕工艺!搭载小爱同学及MIUI+!',5499.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d7dcf06427ba4b189676600ffaf36c4b',0,'2021-03-22 07:00:51','2021-03-22 07:00:51'),(1373892810504335362,1373891219625799682,'英特尔（Intel）i5-10600KF','英特尔（Intel）i5-10600KF 6核12线程 盒装CPU处理器','【游戏5GHz“芯”十代】LGA1200芯片接口,六核十二线程,睿频可至4.8GHz!推荐联合购买Z490主板享受超频快感!',1299.00,'http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/d47152e446674e6da64c77dda1f7a72d',1,'2021-03-22 07:02:26','2021-03-22 07:02:26');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL COMMENT 'id主键',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `user_img` varchar(255) DEFAULT NULL COMMENT '头像',
  `user_status` int(11) DEFAULT '0' COMMENT '用户状态：0-可用，1-禁用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `est` int(11) NOT NULL DEFAULT '1' COMMENT '测试用的列',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1372720720463921154,'张三','e10adc3949ba59abbe56e057f20f883e','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',0,'2021-03-19 01:24:58','2021-03-19 01:24:58',1),(1372747557260992513,'jack','e99a18c428cb38d5f260853678922e03',NULL,0,'2021-03-19 03:11:37','2021-03-19 03:11:37',1),(1372748256292057089,'amy','1344c1a4c0574551a99df86d8769741f','http://qpuevq2yq.hn-bkt.clouddn.com/hlm/images/f9b8bdc6941f4c16826ebbdae51b6be4',0,'2021-03-19 03:14:23','2021-03-19 03:14:23',1),(1372813512586526721,'user','e10adc3949ba59abbe56e057f20f883e','http://r9uv6d9fi.hn-bkt.clouddn.com/hlm/images/59a06fb85edf4f1e83c612dd580d07b8',0,'2021-03-19 07:33:42','2021-03-19 07:33:42',1),(1372813568987332610,'oppo','e10adc3949ba59abbe56e057f20f883e','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',0,'2021-03-19 07:33:55','2021-03-19 07:33:55',1),(1372813597747675138,'vivo','e10adc3949ba59abbe56e057f20f883e','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',0,'2021-03-19 07:34:02','2021-03-19 07:34:02',1),(1372813629330784257,'xiaomi','e10adc3949ba59abbe56e057f20f883e','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',0,'2021-03-19 07:34:10','2021-03-19 07:34:10',1),(1372813671529676802,'apple','e10adc3949ba59abbe56e057f20f883e','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',0,'2021-03-19 07:34:20','2021-03-19 07:34:20',1),(1372813696678723586,'huawei','e10adc3949ba59abbe56e057f20f883e','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',1,'2021-03-19 07:34:26','2021-03-19 07:34:26',1),(1372813729943748609,'sanxing','e10adc3949ba59abbe56e057f20f883e','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',1,'2021-03-19 07:34:34','2021-03-19 07:34:34',1),(1372813795882401793,'sony','e10adc3949ba59abbe56e057f20f883e','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',0,'2021-03-19 07:34:49','2021-03-19 07:34:49',1),(1372813852979462146,'zhongxin','e10adc3949ba59abbe56e057f20f883e','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',1,'2021-03-19 07:35:03','2021-03-19 07:35:03',1),(1373810739723255810,'nuojiya','e10adc3949ba59abbe56e057f20f883e',NULL,0,'2021-03-19 07:35:03',NULL,1),(1387592264544468993,'user3','e10adc3949ba59abbe56e057f20f883e',NULL,0,'2021-04-29 10:19:11',NULL,1),(1474627110671753217,'admin','21232f297a57a5a743894a0e4a801fc3','http://qpuevq2yq.hn-bkt.clouddn.com/bg-pic.png',0,'2021-12-25 14:24:35','2021-12-25 14:24:35',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_order`
--

DROP TABLE IF EXISTS `user_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_order` (
  `id` bigint(20) NOT NULL COMMENT '订单ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '所属用户',
  `order_status` int(11) DEFAULT '1' COMMENT '0:已失效，1:待付款，2:待收货，3待评价，4已完成',
  `order_price` decimal(10,2) DEFAULT NULL COMMENT '订单价格',
  `address_id` bigint(20) DEFAULT NULL COMMENT '地址主键',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_id` (`user_id`),
  KEY `FK_address_id` (`address_id`),
  CONSTRAINT `FK_address_id` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_order`
--

LOCK TABLES `user_order` WRITE;
/*!40000 ALTER TABLE `user_order` DISABLE KEYS */;
INSERT INTO `user_order` VALUES (2945346646753545,1474627110671753217,0,1300.00,NULL,'2022-03-27 17:46:00','2022-03-27 17:46:00'),(1396398960310095873,1372813512586526721,3,1300.00,1375635327678550016,'2021-05-23 17:33:51','2022-03-13 20:31:32'),(1396400230307598337,1372813512586526721,0,2599.00,1375635327678550016,'2021-05-23 17:38:53','2021-05-23 17:38:53'),(1396639308856823809,1372813512586526721,0,1300.00,1375635327678550016,'2021-05-24 09:28:54','2021-05-24 09:28:54'),(1396645008664301569,1372813512586526721,3,4950.00,1375635327678550016,'2021-05-24 09:51:33','2021-05-24 09:57:41'),(1396645585733423105,1372813512586526721,4,4950.00,1375635327678550016,'2021-05-24 09:53:51','2021-05-24 09:56:22'),(1396646991974502401,1372813512586526721,0,4950.00,1375635327678550016,'2021-05-24 09:59:26','2021-05-24 09:59:26'),(1396652103400820737,1372813512586526721,0,1300.00,1375635327678550016,'2021-05-24 10:19:45','2021-05-24 10:19:45'),(1400798883180843009,1372813568987332610,0,1300.00,1401466359015149568,'2021-06-04 20:57:34','2021-06-04 20:57:34'),(1401465968223457281,1372813568987332610,0,1300.00,1401466359015149568,'2021-06-06 17:08:19','2021-06-06 17:08:19'),(1401466149245423617,1372813568987332610,0,4950.00,1401466359015149568,'2021-06-06 17:09:03','2021-06-06 17:09:03'),(1435889790598909953,1372813512586526721,0,9149.00,1375635327678550016,'2021-09-09 16:56:18','2021-09-09 16:56:18'),(1435890621087879169,1372813512586526721,0,1300.00,1375635327678550016,'2021-09-09 16:59:36','2021-09-09 16:59:36'),(1445209782989492225,1372813512586526721,0,4200.00,1375635327678550016,'2021-10-05 10:10:38','2021-10-05 10:10:38'),(1445210649092296705,1372813512586526721,0,4950.00,1375635327678550016,'2021-10-05 10:14:04','2021-10-05 10:14:04'),(1445218348194467841,1372813512586526721,0,4200.00,1375635327678550016,'2021-10-05 10:44:40','2021-10-05 10:44:40'),(1445248553420197889,1372813512586526721,0,4200.00,1375635327678550016,'2021-10-05 12:44:41','2021-10-05 12:44:41'),(1502914877323350017,1372813512586526721,4,4200.00,NULL,'2022-03-13 15:50:04','2022-03-27 17:19:33');
/*!40000 ALTER TABLE `user_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'shop'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-07 21:55:01
