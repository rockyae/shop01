package com.xxx.dto;

import com.xxx.enums.ApiEnum;
import lombok.Data;

/**
 * @Author: chen
 * @Date: 2021/3/1 4:34 PM
 * @Version 1.0
 */
@Data
/*
    这个result类似一种规范，即返回给前台的json对象要包含code,message,data;
 */
public class ApiResult {

    private int code;

    private String message;

    private Object data;

    public ApiResult(){

    }

    public ApiResult(int code, String message, Object data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiResult(ApiEnum apiEnum , Object data){
        this.code = apiEnum.getCode();
        this.message = apiEnum.getMessage();
        this.data = data;
    }

    public ApiResult(ApiEnum apiEnum){
        this.code = apiEnum.getCode();
        this.message = apiEnum.getMessage();
        this.data = null;
    }

    public ApiResult(String message){
        this.code = 500;
        this.message = message;
        this.data = null;
    }
}
