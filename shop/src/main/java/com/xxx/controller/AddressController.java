package com.xxx.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxx.dto.ApiResult;
import com.xxx.entity.Address;
import com.xxx.service.IAddressService;
import com.xxx.utils.R;
import com.xxx.utils.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 地址表 前端控制器
 * </p>
 */
@RestController
@RequestMapping("/address")
@Api(tags = "地址相关接口")
public class AddressController {

    @Autowired
    private IAddressService addressService;

    @PostMapping("/list")
    @ApiOperation("用户地址列表")
    @CrossOrigin
    public ApiResult list(){
        QueryWrapper<Address> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", UserUtils.getUser().get("id"));
        queryWrapper.orderByAsc("is_default");
        List<Address> list = addressService.list(queryWrapper);
        return R.ok(list);
    }

    @PostMapping("/changeDefault/{id}")
    @ApiOperation("改变用户默认地址")
    @CrossOrigin
    public ApiResult changeDefault(@PathVariable String id){
        addressService.changeDefault(id);
        return R.ok();
    }
    @GetMapping("/queryAddressDesc/{id}")
    @ApiOperation("查询用户地址详情")
    @CrossOrigin
    public ApiResult queryAddressDesc(@PathVariable String id){
        Address address = addressService.queryAddress(Long.valueOf(id));
        return R.ok(address);
    }
    @PostMapping("/add")
    @ApiOperation("添加用户地址")
    @CrossOrigin
    public ApiResult add(@RequestBody Address address){
        addressService.add(address);
        return R.ok();
    }
    @PostMapping("/delete/{id}")
    @ApiOperation("删除用户地址")
    @CrossOrigin
    public ApiResult delete(@PathVariable String id){
        addressService.removeById(id);
        return R.ok();
    }
    @PostMapping("/update")
    @ApiOperation("修改用户地址")
    @CrossOrigin
    public ApiResult update(@RequestBody Address address){
        addressService.updateAddress(address);
        return R.ok();
    }


}
