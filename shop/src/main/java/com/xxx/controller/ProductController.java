package com.xxx.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xxx.dto.ApiResult;
import com.xxx.entity.Product;
import com.xxx.entity.request.ProductRequest;
import com.xxx.entity.response.ProductResponse;
import com.xxx.service.IProductService;
import com.xxx.utils.IdWorker;
import com.xxx.utils.QiniuUtil;
import com.xxx.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 */
@RestController
@RequestMapping("/product")
@Api(tags = "商品控制器")

public class ProductController {

    private static final String perfix = "http://r9uv6d9fi.hn-bkt.clouddn.com/";


    @Autowired
    private IProductService productService;

    @RequestMapping(value = "/doUpLoad",method = RequestMethod.POST)
    @ApiOperation("上传文件")
    @CrossOrigin
    public ApiResult upload(@RequestBody MultipartFile file) throws IOException {
        //新名字
        String newFileName = UUID.randomUUID().toString().replaceAll("-", "");
        String fileSrc = QiniuUtil.byteUpLoad(file.getBytes(), newFileName);
        return R.ok(perfix +fileSrc);
    }

    @ApiOperation("查询商品列表")
    @CrossOrigin
    @RequestMapping(value = "/list",method = RequestMethod.POST)
    public ApiResult list(@RequestBody ProductRequest request){
        IPage<ProductResponse> page = productService.findListForPage(request);
        return R.ok(page);
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiOperation("添加商品")
    @CrossOrigin
    public ApiResult add(@RequestBody ProductRequest request ){
        Product product = new Product();
        //拷贝参数到实体类中
        BeanUtils.copyProperties(request,product);
        productService.save(product);
        return R.ok();
    }
    @RequestMapping(value = "/modify",method = RequestMethod.POST)
    @CrossOrigin
    public ApiResult modify(@RequestBody ProductRequest request ){
        Product product = new Product();
        //拷贝参数到实体类中
        BeanUtils.copyProperties(request,product);
        productService.saveOrUpdate(product);
        return R.ok();
    }

    @ApiOperation("上架下架")
    @RequestMapping(value = "/upOrDown",method = RequestMethod.POST)
    @CrossOrigin
    public ApiResult upOrDown(@RequestBody ProductRequest request){
        productService.upOrDown(request.getId());
        return R.ok();
    }
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @CrossOrigin
    public ApiResult delete(@RequestBody Long  productId){
        if(productId == null){
            return R.fail("未传入产品id！");
        }
        if(productService.getById(productId).getProductStatus() == 0){
            return R.fail("商品未下架不能删除！");
        }
        productService.removeById(productId);
        return R.ok();
    }


}
