package com.xxx.controller;


import com.xxx.dto.ApiResult;
import com.xxx.entity.Category;
import com.xxx.enums.ApiEnum;
import com.xxx.exception.GlobalException;
import com.xxx.service.ICategoryService;
import com.xxx.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@RestController
@RequestMapping("/category")
@Api(tags = "分类控制器")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    @ApiOperation("查询所有分类")
    @PostMapping("/list")
    @CrossOrigin
    public ApiResult list(){
        List<Category> list = categoryService.list();
        return R.ok(list);
    }

    @ApiOperation("添加分类")
    @PostMapping("/add")
    @CrossOrigin
    public ApiResult add(@RequestBody Category category){
        if(category == null||category.getCategoryName().equals("")){
            throw new GlobalException(ApiEnum.CATE_NAME_NULL);
        }
        category.setCreateTime(new Date());
        category.setUpdateTime(new Date());
         categoryService.save(category);
        return R.ok();
    }

}
