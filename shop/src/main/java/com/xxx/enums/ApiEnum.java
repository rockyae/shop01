package com.xxx.enums;

import lombok.Getter;

/**
 * @Author: chen
 * @Date: 2021/3/1 5:05 PM
 * @Version 1.0
 */
@Getter
/*
封装并返回各种操作结果
结果都类似 状态码+消息 的格式，而且是常量，所以考虑用枚举类实现；
 */
public enum ApiEnum {

    SUCCESS(200,"成功"),
    FAIL(500,"失败"),
    SELECT_TOO_MANY(1000,"查询出现TOO_MANY错误"),
    USERNAME_OR_PASSWORD_IS_NULL(10000,"用户名或密码为空"),
    USERNAME_ALREADY_EXIST(10001,"用户名被注册"),
    VERIFICATIONCODE_OR_UUID_IS_NULL(10002,"验证码为空"),
    VERIFICATIONCODE_IS_ERROR(10003,"验证码错误"),
    USERNAME_OR_PASSWORD_IS_ERROR(10004,"用户名或密码错误"),
    HEANDER_IS_NULL(10005,"token头部信息为空"),
    TOKEN_IS_ERROR(10006,"错误的token"),
    ADDRESS_TOO_MANY(10007,"地址太多"),
    DEFAULT_ALREADY_EXIST(10008,"用户默认地址只能存在一个"),
    CATE_NAME_NULL(10009,"分类名称为空"),
    ERROR(10010,"金额格式错误，最多保留2位小数"),
    LACK_STOCK(10011,"当前的产品库存不足")
    ;
    private int code;

    private String message;
    //此处显式指定了有参构造函数，必须要传入参数完成构造
    ApiEnum(int code, String message){
        this.code = code;
        this.message = message;
    }

}
