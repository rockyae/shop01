package com.xxx.enums;

import lombok.Getter;

/**
 * @Author: chen
 * @Date: 2021/3/23 10:12 AM
 * @Version 1.0
 */
@Getter
public enum  OrderEnum {
    //0:已失效，1:待付款，2:待收货，3待评价，4已完成
    A(0),B(1),C(2),D(3),E(4);

    private int code;

    OrderEnum(int code){
        this.code = code;
    }

}
