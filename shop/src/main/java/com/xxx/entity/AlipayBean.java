package com.xxx.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author: chen
 * @Date: 2021/3/23 5:46 PM
 * @Version 1.0
 */
/*支付实体对象*/
@Data
@Accessors(chain = true)
public class AlipayBean implements Serializable {

    @ApiModelProperty("商户订单号，必填")
    private String out_trade_no;

    @ApiModelProperty("订单名称，必填")
    private String subject;

    @ApiModelProperty("付款金额，必填")
    private String total_amount;

    @ApiModelProperty("商品描述，可空")
    private String body;

    @ApiModelProperty("公用回传参数，如果请求时传递了该参数，则返回给商户时会回传该参数。支付宝会在异步通知时将该参数原样返回。本参数必须进行 UrlEncode 之后才可以发送给支付宝。")
    private String passback_params;

    @ApiModelProperty("超时时间参数")
    private String timeout_express="10m";

    private String product_code="FAST_INSTANT_TRADE_PAY";
}