package com.xxx.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: AllenXiao
 * @Date: 2022/4/9 15:25
 */

@Data
@Builder
public class ChargeRecord implements Serializable {
    private long Id;

    private long userId;

    private BigDecimal amount;

    private BigDecimal balance;

    private Date createTime;

    private Date updateTime;
}
