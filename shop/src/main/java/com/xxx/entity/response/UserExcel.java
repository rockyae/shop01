package com.xxx.entity.response;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: chen
 * @Date: 2021/3/19 3:57 PM
 * @Version 1.0
 */
@Data
//@EqualsAndHashCode(callSuper = false)
//@Accessors(chain = true)
//@ApiModel(value="用户表格响应类", description="用户")
@ColumnWidth(25)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class UserExcel implements Serializable {

    @ColumnWidth(15)
    @ExcelProperty("用户id")
    private String id;

    @ColumnWidth(15)
    @ExcelProperty("用户名")
    private String username;

    @ColumnWidth(15)
    @ExcelProperty("创建时间")
    private Date createTime;


}
