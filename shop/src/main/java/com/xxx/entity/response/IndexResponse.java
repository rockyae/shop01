package com.xxx.entity.response;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author: chen
 * @Date: 2021/3/26 4:33 PM
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="index", description="")
public class IndexResponse implements Serializable {

    private BigDecimal totalSales;

    private Integer totalUser;

    private Integer orderGoods;

    private Integer notOrderGoods;

    private List<String> timeSales;

    private List<BigDecimal> weekSales;

    private List<String> timeGoods;

    private List<Integer> weekGoods;



}
