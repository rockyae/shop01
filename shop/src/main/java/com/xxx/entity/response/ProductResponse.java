package com.xxx.entity.response;

import com.xxx.entity.Product;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author: chen
 * @Date: 2021/3/18 10:27 AM
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Product请求响应类", description="商品列表")
public class ProductResponse extends Product implements Serializable {

    @ApiModelProperty("分类名称")
    private String categoryName;

}
