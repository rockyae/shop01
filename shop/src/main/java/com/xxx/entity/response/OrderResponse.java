package com.xxx.entity.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xxx.entity.Address;
import com.xxx.entity.OrderProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author: chen
 * @Date: 2021/3/23 2:38 PM
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Category对象", description="")
public class OrderResponse implements Serializable {


    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "0:已失效，1:待付款，2:待收货，3待评价，4已完成")
    private Integer orderStatus;

    @ApiModelProperty(value = "订单价格")
    private BigDecimal orderPrice;

    @ApiModelProperty("订单商品")
    private List<OrderProduct> products;

    @ApiModelProperty("用户地址")
    private Address address;

    @ApiModelProperty("所属人id")
    private String userId;

    @ApiModelProperty("所属人")
    private String username;

//    @ApiModelProperty(value = "地址主键")
//    private Long addressId;

    private Date createTime;

    private Date updateTime;

}
