package com.xxx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xxx.VIP.MemberInterface;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author chen
 * @since 2021-03-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="User对象", description="")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Transient
    private MemberInterface memberInterface;

    @ApiModelProperty(value = "id主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "头像")
    private String userImg;

    @ApiModelProperty(value = "用户状态：0-可用，1-禁用")
    private Integer userStatus;

    private BigDecimal balance = new BigDecimal(0.0);


    //用户会员等级：0-普通会员，无折扣；1-黄金会员，打9折；2-钻石会员，打7折
    private int vipLevel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    //策略模式
    public BigDecimal getPrice(BigDecimal price){
        return this.memberInterface.getPrice(price);
    }


}
