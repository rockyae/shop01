package com.xxx.entity.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author: chen
 * @Date: 2021/3/18 10:19 AM
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="分页参数请求父类", description="分页")
public class BaseRequest implements Serializable {

    @ApiModelProperty(value = "页数",example = "1")
    private int page = 1;

    @ApiModelProperty(value = "每页显示数量",example = "10")
    private int size = 500;

}
