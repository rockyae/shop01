package com.xxx.entity.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: chen
 * @Date: 2021/3/19 9:17 AM
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)

public class UserRequest extends BaseRequest implements Serializable {

    @ApiModelProperty("用户id")
    private Long id;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("验证码校验码")
    private String uuid;

    @ApiModelProperty("验证码")
    private String verificationCode;

    @ApiModelProperty("头像")
    private String userImg;

    @ApiModelProperty("开始时间")
    private Date startTime;

    @ApiModelProperty("结束时间")
    private Date endTime;

    @ApiModelProperty(value = "用户状态：0-可用，1-禁用")
    private Integer userStatus;


}
