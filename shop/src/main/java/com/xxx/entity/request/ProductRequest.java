package com.xxx.entity.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: chen
 * @Date: 2021/3/18 10:19 AM
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Product请求查询类", description="后台商品列表")
public class ProductRequest extends BaseRequest implements Serializable {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("分类id")
    private Long cateId;

    @ApiModelProperty("商品名称")
    private String productName;

    @ApiModelProperty("商品标题")
    private String productTitle;

    @ApiModelProperty("商品描述")
    private String productDesc;

    @ApiModelProperty("商品价格")
    private BigDecimal productPrice;

    @ApiModelProperty("商品图片")
    private String productImg;

    @ApiModelProperty("价格的最低价")
    private BigDecimal priceLow;

    @ApiModelProperty("价格的最高价")
    private BigDecimal priceHigh;

    @ApiModelProperty(value = "商品状态：0-上架，1-下架")
    private Integer productStatus;

    //商品库存
    private int stock;

    @ApiModelProperty("开始时间")
    private Date startTime;

    @ApiModelProperty("结束时间")
    private Date endTime;


}
