package com.xxx.entity.request;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
@Data
@Accessors(chain = true)
public class ChargeRequest {

    //private Long id;//充值订单号

    private BigDecimal money;

}
