package com.xxx.entity.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author: chen
 * @Date: 2021/3/23 9:30 AM
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="order请求类", description="订单")
public class OrderRequest extends BaseRequest implements Serializable {

    @ApiModelProperty("商品id")
    private Long id;

    @ApiModelProperty("商品数量")
    private Integer num;

}
