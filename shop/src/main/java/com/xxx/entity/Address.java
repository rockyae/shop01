package com.xxx.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 地址表
 * </p>
 *
 * @author chen
 * @since 2021-03-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Address对象", description="地址表")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "名字")
    private String nickname;

    @ApiModelProperty(value = "电话")
    private String telephone;

    @ApiModelProperty(value = "详细地址")
    private String addressDesc;

    @ApiModelProperty(value = "邮政编码")
    private String postalCode;

    @ApiModelProperty(value = "是否为默认地址0:默认1不是")
    private String isDefault;

    @ApiModelProperty(value = "地址使用计数器")
    private Integer addressNum;

    private Date createTime;

    private Date updateTime;


}
