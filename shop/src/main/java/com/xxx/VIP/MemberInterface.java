package com.xxx.VIP;


import java.math.BigDecimal;

//会员服务抽象接口
public interface MemberInterface {

    //返回折扣后的价格
    public BigDecimal getPrice(BigDecimal price);

}
