package com.xxx.VIP;

import java.math.BigDecimal;

public class GoldMember implements MemberInterface{
    @Override
    public BigDecimal getPrice(BigDecimal price) {
        return price.multiply(new BigDecimal(0.9));
    }
}
