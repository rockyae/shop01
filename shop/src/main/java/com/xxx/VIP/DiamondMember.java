package com.xxx.VIP;

import java.math.BigDecimal;

public class DiamondMember implements MemberInterface{
    @Override
    public BigDecimal getPrice(BigDecimal price) {
        return price.multiply(new BigDecimal(0.7));
    }
}
