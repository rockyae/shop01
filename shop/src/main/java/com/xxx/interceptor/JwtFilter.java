package com.xxx.interceptor;

import com.xxx.entity.User;
import com.xxx.enums.ApiEnum;
import com.xxx.exception.GlobalException;
import com.xxx.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;

/**
 * @Author: chen
 * @Date: 2021/3/23 9:38 AM
 * @Version 1.0
 */
@Component
/*
该过滤器实现用户token校验
JWT是由三段信息构成的，将这三段信息文本用.链接一起就构成了Jwt字符串。就像这样:
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
第一部分我们称它为头部（header),第二部分我们称其为载荷（payload, 类似于飞机上承载的物品)，第三部分是签证（signature).
 */
public class JwtFilter implements HandlerInterceptor {

    private static final String noTokne = "/user/login,/user/verificationCode,/user/register,/doc.html," +
            "/webjars/bycdao-ui/images/api.ico,/swagger-resources,/webjars/bycdao-ui/ace-editor/theme-eclipse.js," +
            "/webjars/bycdao-ui/ace-editor/mode-json.js,/order/alipay_callback,/order/alipay_notify,/favicon.ico,/order/alipay_callback";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        //放行预检请求，防止出现跨域配置不生效问题
        if(request.getMethod().equalsIgnoreCase("OPTIONS")){
            return true;
        }

        //排除不需要token的接口
        String servletPath = request.getServletPath();
        System.out.println("servletPath = " + servletPath);
        for(String string : noTokne.split(",")){
            if(servletPath.equals(string)){
                return true;
            }
        }
        //检测token头是否有效或者是否为空
        String token = request.getHeader(JwtUtils.TOKEN_HEADER);
        if(StringUtils.isBlank(token)){
            throw new GlobalException(ApiEnum.HEANDER_IS_NULL);
        }
        Claims claims = JwtUtils.parseJWT(token);
        LinkedHashMap user = claims.get("user", LinkedHashMap.class);
        request.setAttribute("user",user);
        return true;
    }
}
