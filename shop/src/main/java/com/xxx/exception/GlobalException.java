package com.xxx.exception;

import com.xxx.enums.ApiEnum;
import lombok.Data;

/**
 * @Author: chen
 * @Date: 2021/3/2 10:11 AM
 * @Version 1.0
 */
//需求针对业务进行异常抛出
@Data
public class GlobalException extends RuntimeException{

    private ApiEnum apiEnum;

    public GlobalException(){
        super();
    }

    public GlobalException(ApiEnum apiEnum){
        super(apiEnum.getMessage());
        this.apiEnum = apiEnum;
    }

}
