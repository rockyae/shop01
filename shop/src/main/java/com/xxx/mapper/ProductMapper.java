package com.xxx.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxx.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxx.entity.request.ProductRequest;
import com.xxx.entity.response.ProductResponse;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author chen
 * @since 2021-03-18
 */
public interface ProductMapper extends BaseMapper<Product> {

    Page<ProductResponse> selectListForPage(@Param("productIPage") IPage<ProductResponse> productIPage,
                                            @Param("request") ProductRequest request);

    int decreaseStockForVersion(@Param("productId")Long productId, @Param("num")Integer num,@Param("version") Integer version);

    int increaseStockForVersion(@Param("productId")Long productId, @Param("num")Integer num,@Param("version") Integer version);
}
