package com.xxx.mapper;

import com.xxx.entity.OrderProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chen
 * @since 2021-03-23
 */
public interface OrderProductMapper extends BaseMapper<OrderProduct> {

    List<OrderProduct> selectOrder(@Param("id") Long id);
}
