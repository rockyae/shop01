package com.xxx.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxx.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxx.entity.request.UserRequest;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chen
 * @since 2021-03-19
 */
public interface UserMapper extends BaseMapper<User> {

    User selectUserName(@Param("username") String username);

    Page<User> selectUserForPage(@Param("page") IPage<User> page, @Param("request") UserRequest request);

    User selectByIdUser(@Param("userId") long userId);

    int updateById(User user);
}
