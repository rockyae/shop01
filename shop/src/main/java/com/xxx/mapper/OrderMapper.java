package com.xxx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxx.entity.UserOrder;
import com.xxx.entity.request.OrderRequest;
import com.xxx.entity.response.OrderResponse;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chen
 * @since 2021-03-23
 */
public interface OrderMapper extends BaseMapper<UserOrder> {

    void insertX(@Param("userOrder") UserOrder userOrder);

    List<UserOrder> selectUserOrder(@Param("id") Long id, @Param("status")Integer OrderStatus);

    List<UserOrder> selectOrderTypeByTime(@Param("date") Date date, @Param("code") int code);

    List<UserOrder> selectByTIme(int code);

    Page<OrderResponse> selectUserOrderAll(@Param("page") Page<OrderResponse> page, @Param("orderRequest") OrderRequest orderRequest);

    BigDecimal sumToBuy(@Param("startTime") Date startTime, @Param("endTime") Date endTime);
    //管理端
    Integer countOrderGoods(@Param("startTime") Date startTime, @Param("endTime") Date endTime);
    Integer countNotOrderGoods();
}
