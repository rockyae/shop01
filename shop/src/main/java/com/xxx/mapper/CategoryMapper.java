package com.xxx.mapper;

import com.xxx.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chen
 * @since 2021-03-22
 */

public interface CategoryMapper extends BaseMapper<Category> {

}
