package com.xxx.mapper;

import com.xxx.entity.Address;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 地址表 Mapper 接口
 * </p>
 *
 * @author chen
 * @since 2021-03-23
 */

public interface AddressMapper extends BaseMapper<Address> {

    Address selectUserDefaultAddress(@Param("id") Long id);

    void clearAddressDefault(@Param("userId") Long userId);

    void setAddressDefault(@Param("id") String id, @Param("userId") Long userId);

    Address selectAddressDescAddress(@Param("id") Long id);
}
