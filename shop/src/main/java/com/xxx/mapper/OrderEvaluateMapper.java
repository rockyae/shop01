package com.xxx.mapper;

import com.xxx.entity.OrderEvaluate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chen
 * @since 2021-03-26
 */
public interface OrderEvaluateMapper extends BaseMapper<OrderEvaluate> {

}
