package com.xxx.job;

import com.alipay.api.AlipayApiException;
import com.xxx.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.TimeUnit;


@Component
public class OrderJob {

    public static final String CLEARORDER = "clearOrder";

    @Autowired
    private IOrderService orderService;

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    /**
     * 清理15分钟还未支付的订单
     */
    //每1分钟执行一次定时任务
    @Scheduled(cron = "0 0/1 * * * ? ")
    public void clearOrder() throws AlipayApiException {
        //需要高频检查但是不需要每次检查都执行清理订单的操作
//        Object clearOrder = redisTemplate.opsForValue().get(CLEARORDER);
//        if(clearOrder != null){
//            return;
//        }
//        redisTemplate.opsForValue().set(CLEARORDER,UUID.randomUUID().toString(),6, TimeUnit.MINUTES);
        orderService.clearOrder();
    }

}
