package com.xxx.utils;

import com.xxx.dto.ApiResult;
import com.xxx.enums.ApiEnum;

/**
 * @Author: chen
 * @Date: 2021/3/1 5:13 PM
 * @Version 1.0
 */
public class R {

    //构造返回成功的方法  有data  和没 data 的两中情况
    public static ApiResult ok(){
        return new ApiResult(ApiEnum.SUCCESS);
    }
    public static ApiResult ok(Object data){
        return new ApiResult(ApiEnum.SUCCESS,data);
    }

    public static ApiResult ok(ApiEnum apiEnum){
        return new ApiResult(apiEnum);
    }

    //构造失败的方法  没 data
    public static ApiResult fail(){
        return new ApiResult(ApiEnum.FAIL);
    }

    public static ApiResult fail(ApiEnum apiEnum){
        return new ApiResult(apiEnum);
    }

    public static ApiResult fail(String message){
        return new ApiResult(message);
    }

}
