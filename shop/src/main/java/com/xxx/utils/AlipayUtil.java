package com.xxx.utils;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.xxx.entity.AlipayBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@PropertySource("classpath:alipay.properties")
/* 支付宝 */
public class AlipayUtil {
    @Value("${gatewayUrl}")
    public   String gatewayUrl;
    @Value("${app_id}")
    public   String app_id;
    @Value("${private_key}")
    public   String private_key;
    @Value("${alipay_public_key}")
    public   String alipay_public_key;
    @Value("${notify_url}")
    public   String notify_url;
    @Value("${return_url}")
    public   String return_url;
    @Value("${sign_type}")
    public   String sign_type;
    @Value("${charset}")
    public   String charset;

    private  AlipayClient getAlipayClient() {
        return new DefaultAlipayClient(
                gatewayUrl,//支付宝网关
                app_id,//appid
                private_key,//应用私钥
                "json",
                charset,//字符编码格式
                alipay_public_key,//支付宝公钥
                sign_type//签名方式
        );
    }

    public  String connect(AlipayBean alipayBean) throws AlipayApiException {
        //1、获得初始化的AlipayClient（客户端）
        AlipayClient alipayClient = getAlipayClient();
        //2、设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        //页面跳转同步通知页面路径
       // alipayRequest.setReturnUrl(return_url);
        // 服务器异步通知页面路径
        alipayRequest.setNotifyUrl(notify_url);
        //封装参数
        alipayRequest.setBizContent(JSON.toJSONString(alipayBean));
        //3、请求支付宝进行付款，并获取支付结果
        String result = alipayClient.pageExecute(alipayRequest).getBody();
        System.out.println(alipayClient);
        //返回付款信息
        return result;
    }

    //查询订单
    public  AlipayTradeQueryResponse search(Map<String,String> map) throws AlipayApiException {
        //1、获得初始化的AlipayClient
        AlipayClient alipayClient = getAlipayClient();
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        request.setBizContent(JSON.toJSONString(map));
        AlipayTradeQueryResponse response = alipayClient.execute(request);
        //返回付款信息
        return response;
    }



}
