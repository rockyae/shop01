package com.xxx.utils;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;

/**
 * @Author: chen
 * @Date: 2021/3/26 10:16 AM
 * @Version 1.0
 */
public class UserUtils {


    public static LinkedHashMap getUser(){
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.
                        currentRequestAttributes ()).getRequest ();
        return (LinkedHashMap) request.getAttribute("user");

    }

}
