package com.xxx.utils;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.storage.persistent.FileRecorder;
import com.qiniu.util.Auth;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class QiniuUtil {

    //设置好账号的ACCESS_KEY和SECRET_KEY
    private static final String ACCESS_KEY = "h6ZwfG2IKvccVd4EkucqPdhkTPVZ3VPFJr71v3uC";
    private static final String SECRET_KEY = "wZo25u4c2Tq87G-Egxs7UbWZnnYEEHxA3f5UB8GK";
    //要上传的空间 bucket name
    private static final String BUCKET_NAME = "onlineshop001";

    /**
     * 简单上传
     * @param localFilePath
     * @param fileName
     * @return
     * @throws QiniuException
     */
    public static String easyUpLoad(File localFilePath, String fileName) throws QiniuException {
        return intoResult(null,localFilePath,null, fileName,null);
    }

    /**
     * 字节数组上传
     * @param uploadBytes
     * @param fileName
     * @return
     * @throws QiniuException
     */
    public static String byteUpLoad(byte[] uploadBytes, String fileName) throws QiniuException {
        return intoResult(uploadBytes,null,null, fileName,null);
    }


    /**
     * 数据流上传
     * @param byteInputStream
     * @param fileName
     * @return
     * @throws QiniuException
     */
    public static String byteArrayInputStreamUpLoad(ByteArrayInputStream byteInputStream, String fileName) throws QiniuException {
        return intoResult(null,null,byteInputStream, fileName,null);
    }

    /**
     * 断点续上传
     * @param localFilePath
     * @param fileName
     * @return
     */
    public static String recorderUpLoad(File localFilePath, String fileName) {
        String localTempDir = Paths.get(System.getenv("java.io.tmpdir"), BUCKET_NAME).toString();
        try {
            //设置断点续传文件进度保存目录
            FileRecorder fileRecorder = new FileRecorder(localTempDir);
            return intoResult(null,localFilePath,null,fileName,fileRecorder);
        } catch (IOException ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }
    }

    private static String intoResult(byte[] uploadBytes, File localFilePath, ByteArrayInputStream byteInputStream, String fileName, FileRecorder fileRecorder) throws QiniuException {
        Configuration cfg = new Configuration(Zone.zone2());
        UploadManager uploadManager = new UploadManager(cfg, fileRecorder);
//        String substring = fileName.substring(fileName.lastIndexOf("."));
        //上传数据保存的文件名
        String key = "hlm/images/"+fileName;
        Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
        String upToken = auth.uploadToken(BUCKET_NAME);
        Response response = null;
        try {
            if(uploadBytes != null && localFilePath == null && byteInputStream == null){
                response = uploadManager.put(uploadBytes, key, upToken);
            }
            if(localFilePath != null && uploadBytes == null && byteInputStream == null){
                response = uploadManager.put(localFilePath, key, upToken);
            }
            if(byteInputStream != null  && uploadBytes == null && localFilePath == null){
                response = uploadManager.put(byteInputStream,key,upToken,null, null);
            }
            if(response == null){
                return "参数错误";
            }
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            return putRet.key;
        } catch (QiniuException ex) {
            Response r = ex.response;
            throw new QiniuException(r);
        }
    }
}
