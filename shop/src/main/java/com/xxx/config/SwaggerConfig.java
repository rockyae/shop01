package com.xxx.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author: chen
 * @Date: 2021/3/2 11:17 AM
 * @Version 1.0
 */
//设置/配置类的类的时候使用
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    //把方法的返回值加入到Spring ioc容器
    @Bean
    public Docket docketConfig(){
        return new Docket(DocumentationType.SWAGGER_2).
                apiInfo(new ApiInfoBuilder().
                        title("api文档标题").
                        description("这个是文档的描述")
                        .version("v1.0.0")
                        .build())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

}
