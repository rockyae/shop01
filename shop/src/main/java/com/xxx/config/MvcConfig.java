package com.xxx.config;

import com.xxx.interceptor.JwtFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author: chen
 * @Date: 2021/3/23 10:42 AM
 * @Version 1.0
 */
@Slf4j
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    //注册拦截器
    @Override
        public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new JwtFilter());
        log.info("注册拦截器成功");
    }
}
