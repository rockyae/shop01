package com.xxx.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: chen
 * @Date: 2021/3/10 9:08 AM
 * @Version 1.0
 */
@Configuration
public class DataSourceConfig {

    @Value("${grape.drive}")
    private String driver;

    @Value("${grape.url}")
    private String url;

    @Value("${grape.username}")
    private String username;

    @Value("${grape.password}")
    private String password;

    @Bean
    public DruidDataSource dataSource(){
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(driver);
        druidDataSource.setUrl(url);
        druidDataSource.setUsername(username);
        druidDataSource.setPassword(password);
        return druidDataSource;
    }

}
