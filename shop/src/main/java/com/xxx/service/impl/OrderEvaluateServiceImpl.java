package com.xxx.service.impl;

import com.xxx.entity.OrderEvaluate;
import com.xxx.mapper.OrderEvaluateMapper;
import com.xxx.service.IOrderEvaluateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chen
 * @since 2021-03-26
 */
@Service
public class OrderEvaluateServiceImpl extends ServiceImpl<OrderEvaluateMapper, OrderEvaluate> implements IOrderEvaluateService {

}
