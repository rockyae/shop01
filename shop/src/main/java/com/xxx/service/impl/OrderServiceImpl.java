package com.xxx.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.xxx.VIP.DiamondMember;
import com.xxx.VIP.GoldMember;
import com.xxx.VIP.NormalMember;
import com.xxx.entity.*;
import com.xxx.entity.request.OrderRequest;
import com.xxx.entity.response.OrderResponse;
import com.xxx.enums.ApiEnum;
import com.xxx.enums.OrderEnum;
import com.xxx.exception.GlobalException;
import com.xxx.job.OrderJob;
import com.xxx.mapper.*;
import com.xxx.service.IOrderProductService;
import com.xxx.service.IOrderService;
import com.xxx.service.IProductService;
import com.xxx.service.PayService;
import com.xxx.utils.AlipayUtil;
import com.xxx.utils.IdWorker;
import com.xxx.utils.UserUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chen
 * @since 2021-03-23
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, UserOrder> implements IOrderService {

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private ProductMapper productMapper;

    @Resource
    private IOrderProductService orderProductService;

    @Resource
    private OrderProductMapper orderProductMapper;

    @Resource
    private AddressMapper addressMapper;

    @Autowired
    private PayService payService;

    @Resource
    private OrderEvaluateMapper orderEvaluateMapper;

    @Resource
    private UserMapper userMapper;

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Autowired
    private AlipayUtil alipayUtil;

    @Autowired
    private IProductService productService;

//    @Autowired
//    private User user;

    @Override
    public Boolean tryMinusStock(List<OrderRequest> orderRequest) {
        for(OrderRequest o : orderRequest){
            if(!productService.sell(o.getId(),o.getNum())){
                return false;
            }
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long getOrder(List<OrderRequest> orderRequest) {
        //这是一个id生成器
        IdWorker idWorker = new IdWorker(1,1,1);
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.
                        currentRequestAttributes ()).getRequest ();
        LinkedHashMap user = (LinkedHashMap)request.getAttribute("user");
        BigDecimal price = BigDecimal.ZERO;

        List<OrderProduct> orderProducts = Lists.newArrayList();
        //requestX对应一个商品
        for(OrderRequest requestX : orderRequest){
            //查询生成订单的商品
            Product product = productMapper.selectById(requestX.getId());
            price = price.add(product.getProductPrice().
                    multiply(new BigDecimal(requestX.getNum())));
            //封装订单商品快照表数据
            OrderProduct orderProduct = new OrderProduct();
            BeanUtils.copyProperties(product,orderProduct);
            orderProduct.setProductId(product.getId());
            //手动设置id
            orderProduct.setId(idWorker.nextId());
            orderProduct.setProductNum(requestX.getNum());
            orderProduct.setCreateTime(new Date());
            orderProduct.setUpdateTime(new Date());
            orderProducts.add(orderProduct);
        }
        long l = idWorker.nextId();
        //封装订单表数据
        UserOrder userOrder = new UserOrder();
        userOrder.setId(l);
        userOrder.setUserId((Long) user.get("id"));
        userOrder.setOrderStatus(OrderEnum.B.getCode());
        User u = userMapper.selectByIdUser((long) user.get("id"));
        int vipLevel = u.getVipLevel();
        if(vipLevel == 0){
            u.setMemberInterface(new NormalMember());
        }else if(vipLevel == 1){
            u.setMemberInterface(new GoldMember());
        }else{
            u.setMemberInterface(new DiamondMember());
        }
        //折扣后价格
        price = u.getPrice(price);
        userOrder.setOrderPrice(price);
        userOrder.setCreateTime(new Date());
        userOrder.setUpdateTime(new Date());
        orderMapper.insert(userOrder);
        orderProducts = orderProducts.stream().peek(orderProduct ->
                orderProduct.setOrderId(userOrder.getId())).
                collect(Collectors.toList());

        orderProductService.saveBatch(orderProducts);

        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setId(l);
        orderResponse.setOrderStatus(OrderEnum.B.getCode());
        orderResponse.setOrderPrice(price);
        orderResponse.setProducts(orderProducts);
        //redisTemplate存储订单信息
        redisTemplate.opsForValue().set(l+"",orderResponse,20, TimeUnit.MINUTES);

        return userOrder.getId();
    }

    @Override
    public OrderResponse orderDesc(String id) {

        //尝试从缓存中获取
        OrderResponse response = (OrderResponse) redisTemplate.opsForValue().get(id);
        if(response != null){
            //获取用户的默认地址
            HttpServletRequest request = ((ServletRequestAttributes)
                    RequestContextHolder.
                            currentRequestAttributes ()).getRequest ();
            LinkedHashMap user = (LinkedHashMap) request.getAttribute("user");
            Address address = addressMapper.selectUserDefaultAddress(Long.parseLong(user.get("id").toString()));
            if(address != null){
                response.setAddress(address);
            }
            return response;

        }

        //查询数据库
        UserOrder userOrder = orderMapper.selectById(id);
        if(userOrder == null){
            throw new GlobalException(ApiEnum.FAIL);
        }

        //根据订单查询所有商品快照
        List<OrderProduct> orderProducts = orderProductMapper.selectOrder(userOrder.getId());

        Address address = addressMapper.selectById(userOrder.getAddressId());
        response = new OrderResponse();
        response.setId(userOrder.getId());
        response.setOrderPrice(userOrder.getOrderPrice());
        response.setOrderStatus(userOrder.getOrderStatus());
        response.setProducts(orderProducts);
        response.setAddress(address);

        redisTemplate.opsForValue().set(userOrder.getId()+"",response,5, TimeUnit.MINUTES);
        return response;
    }

    @Override
    public List<OrderResponse> listOrder(Integer orderStatus) {

        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.
                        currentRequestAttributes ()).getRequest ();
        //获取当前用户id
        LinkedHashMap user = (LinkedHashMap) request.getAttribute("user");

        List<OrderResponse> responses = new ArrayList<>();

        List<UserOrder> userOrders =  orderMapper.selectUserOrder(
                Long.parseLong(user.get("id").toString()),orderStatus);

        for(UserOrder userOrder : userOrders){
            OrderResponse orderResponse = new OrderResponse();
            List<OrderProduct> orderProducts = orderProductMapper.selectOrder(userOrder.getId());
            orderResponse.setProducts(orderProducts);
            orderResponse.setOrderPrice(userOrder.getOrderPrice());
            orderResponse.setId(userOrder.getId());
            orderResponse.setOrderStatus(userOrder.getOrderStatus());
            responses.add(orderResponse);
        }
        return responses;
    }

    @Override
    public String pay(String id,String addressId) throws AlipayApiException {
        UserOrder userOrder = orderMapper.selectById(id);
        BigDecimal price = userOrder.getOrderPrice();
        User u = userMapper.selectByIdUser((long)UserUtils.getUser().get("id"));
        BigDecimal balance = u.getBalance();
        if((balance.doubleValue() - price.doubleValue()) >= 0){
            balance = balance.subtract(price);
            u.setBalance(balance);
            userMapper.updateById(u);
            redisTemplate.opsForValue().set("address-order"+userOrder.getId(),addressId,10,TimeUnit.MINUTES);
            changeOrder(OrderEnum.C,userOrder.getId().toString(),addressId);
            return "success";
        }
        redisTemplate.opsForValue().set("address-order"+userOrder.getId(),addressId,10,TimeUnit.MINUTES);
        return payService.aliPay(new AlipayBean().setBody(addressId)
                .setOut_trade_no(userOrder.getId().toString())
                .setTotal_amount((price.toString()))
                .setSubject(UUID.randomUUID().toString()));

    }

    @Override
    public void changeOrder(OrderEnum c, String out_trade_no,String addressId) {
        redisTemplate.delete(out_trade_no);
        if(StringUtils.isBlank(addressId)){
            addressId = (String) redisTemplate.opsForValue().get("address-order" + out_trade_no);
        }
        UserOrder userOrder = new UserOrder();
        userOrder.setId(Long.parseLong(out_trade_no));
        userOrder.setOrderStatus(c.getCode());
        if(StringUtils.isNotBlank(addressId)){
            userOrder.setAddressId(Long.parseLong(addressId));
        }
        userOrder.setUpdateTime(new Date());
        orderMapper.updateById(userOrder);
        redisTemplate.delete("address-order" + out_trade_no);
    }

    @Override
    //自动取消15分钟前未付款订单并恢复库存
    public void clearOrder() throws AlipayApiException {
        log.debug("正在清理中.......");
        Date date = DateUtils.addMinutes(new Date(), -5);
        //查询5分钟之前的订单（B类订单）
        List<UserOrder> userOrders = orderMapper.selectOrderTypeByTime(date,OrderEnum.B.getCode());
        List<UserOrder> a = new ArrayList<>();
        List<UserOrder> c = new ArrayList<>();

        for(UserOrder userOrder : userOrders){
            Map<String,String> map = new HashMap<>();
            //商户订单号
            map.put("out_trade_no",userOrder.getId().toString());
            AlipayTradeQueryResponse search = alipayUtil.search(map);
            List<OrderProduct> products = orderProductMapper.selectOrder(userOrder.getId());
            //判断未付款订单的状态
            //如果不是10000 的code  == 》  没有点击支付按钮
            if(!search.getCode().equals("10000") ){
                userOrder.setOrderStatus(OrderEnum.A.getCode());
                a.add(userOrder);
                for(OrderProduct o:products){
                    productService.restore(o.getProductId(),o.getProductNum());
                }
                continue;
            }
            // 出现回调接口延时  - 》 修改订单为支付状态
            if(search.getTradeStatus().equalsIgnoreCase("TRADE_SUCCESS")){
                userOrder.setOrderStatus(OrderEnum.C.getCode());
                c.add(userOrder);
                continue;
            }
            //  用户 可能是 余额不足  --》  停止支付
            if(search.getTradeStatus().equalsIgnoreCase("WAIT_BUYER_PAY")){
                userOrder.setOrderStatus(OrderEnum.A.getCode());
                a.add(userOrder);
                for(OrderProduct o:products){
                    productService.restore(o.getProductId(),o.getProductNum());
                }
                //continue;
            }
        }
        //统一更新
        this.updateBatchById(a);
        this.updateBatchById(c);

        //redisTemplate.delete(OrderJob.CLEARORDER);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void evaluate(String id, OrderEvaluate orderEvaluate) {
        if(StringUtils.isBlank(id)){
            throw new GlobalException(ApiEnum.FAIL);
        }
        //改变订单状态
        this.changeOrder(OrderEnum.E,id,null);
        //save 保存评论
        orderEvaluate.setOrderId(Long.parseLong(id));
        orderEvaluate.setCreateTime(new Date());
        orderEvaluate.setUpdateTime(new Date());
        orderEvaluateMapper.insert(orderEvaluate);

    }

    @Override
    public Page<OrderResponse> listOrderAll(OrderRequest orderRequest) {

        Page<OrderResponse> page = new Page<>(orderRequest.getPage(),orderRequest.getSize());
        Page<OrderResponse> userOrders =  orderMapper.selectUserOrderAll(page,orderRequest);

        for(OrderResponse userOrder : userOrders.getRecords()){
            User user = userMapper.selectByIdUser(Long.parseLong(userOrder.getUserId()));
            List<OrderProduct> orderProducts = orderProductMapper.selectOrder(userOrder.getId());
            userOrder.setUsername(user.getUsername());
            userOrder.setProducts(orderProducts);
        }
        return userOrders;
    }

    @Override
    public BigDecimal selectSumToBuy(Date startTime, Date endTime) {
        return orderMapper.sumToBuy(startTime,endTime);
    }

    @Override
    public Integer getOrderSum(int i) {
        if(i == 0){
            //已收货
            return orderMapper.countOrderGoods(null,null);
        }
        //未收货（含订单取消，订单未付款，订单未收货）
        else {
            return orderMapper.countNotOrderGoods();
        }
    }

    @Override
    public Integer getOrderSumPerDay(Date startTime, Date endTime) {
        return orderMapper.countOrderGoods(startTime,endTime);
    }


}
