package com.xxx.service.impl;

import com.alipay.api.AlipayApiException;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxx.entity.User;
import com.xxx.entity.request.UserRequest;
import com.xxx.enums.ApiEnum;
import com.xxx.exception.GlobalException;
import com.xxx.mapper.UserMapper;
import com.xxx.service.IUserService;
import com.xxx.utils.JwtUtils;
import com.xxx.utils.MD5Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chen
 * @since 2021-03-19
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Autowired
    private PayServiceImpl payService;


    private String admin = "admin";

    @Override
    public void register(UserRequest request) {
        //判断用户名或密码是否为空
        if(StringUtils.isBlank(request.getUsername()) ||
                StringUtils.isBlank(request.getPassword()) ){
            throw new GlobalException(ApiEnum.USERNAME_OR_PASSWORD_IS_NULL);
        }

        //判断用户是否存在
        User bean =  userMapper.selectUserName(request.getUsername());
        if(bean != null){
            throw new GlobalException(ApiEnum.USERNAME_ALREADY_EXIST);
        }

        //密码加密 [获得加密后的密码]，加密后的密文被存放在数据库
        String encode = MD5Utils.encode(request.getPassword());

        User user = new User();
        BeanUtils.copyProperties(request,user);
        user.setPassword(encode);
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());

        userMapper.insert(user);
    }

    @Override
    public Map<String, Object> login(UserRequest request) {
        boolean flag = false;
        //校验参数
        if(StringUtils.isBlank(request.getUsername())
                || StringUtils.isBlank(request.getPassword())){
            throw new GlobalException(ApiEnum.USERNAME_OR_PASSWORD_IS_NULL);
        }
        if(StringUtils.isBlank(request.getUuid())
                || StringUtils.isBlank(request.getVerificationCode())){
            throw new GlobalException(ApiEnum.VERIFICATIONCODE_OR_UUID_IS_NULL);
        }
        //校验验证码
        Object obj = redisTemplate.opsForValue().get(request.getUuid());
        if(obj == null){
            throw new GlobalException(ApiEnum.VERIFICATIONCODE_IS_ERROR);
        }
        String code = (String) obj;
        if(!request.getVerificationCode().equalsIgnoreCase(code)){
            throw new GlobalException(ApiEnum.VERIFICATIONCODE_IS_ERROR);
        }
        flag = request.getUsername().equals(admin);
        //查用户
        User user = userMapper.selectUserName(request.getUsername());
        if(user == null ){
            throw new GlobalException(ApiEnum.USERNAME_OR_PASSWORD_IS_ERROR);
        }
        //比对密码
        String encode = MD5Utils.encode(request.getPassword());
        if(!encode.equals(user.getPassword())){
            throw new GlobalException(ApiEnum.USERNAME_OR_PASSWORD_IS_ERROR);
        }
        //生成令牌
        Map<String,Object> map = new HashMap<>();
        map.put("user",user);
        String token = JwtUtils.createJWT(user.getId().toString(), map, user.getUsername(), 60*30*2);
        //构造返回结果
        Map<String,Object> result = new HashMap<>();
        result.put("user",user);
        result.put("token",token);
        if(flag) result.put("flag","admin");
        //作废验证码
        redisTemplate.delete(request.getUuid());
        return result;
    }


//    public String charge(int amount) throws AlipayApiException  {
//        String url =  payService.aliPay(new AlipayBean().setSubject("充值").setTotal_amount(new StringBuffer(Integer.toString(amount))));
//        return url;
//    }

    @Override
    //后端将数据分好页后再传给前端
    public Page<User> findListForPage(UserRequest request) {
        IPage<User> page = new Page<>(request.getPage(),request.getSize());
        return userMapper.selectUserForPage(page,request);
    }




}
