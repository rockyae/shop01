package com.xxx.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxx.entity.Product;
import com.xxx.entity.request.ProductRequest;
import com.xxx.entity.response.ProductResponse;
import com.xxx.mapper.OrderProductMapper;
import com.xxx.mapper.ProductMapper;
import com.xxx.service.IProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author chen
 * @since 2021-03-18
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    @Resource
    private ProductMapper productMapper;

    @Resource
    private OrderProductMapper orderProductMapper;

    @Override
    public Page<ProductResponse> findListForPage(ProductRequest request) {
        IPage<ProductResponse> productIPage = new Page<>(request.getPage(),request.getSize());
        return productMapper.selectListForPage(productIPage,request);
    }

    @Override
    public void upOrDown(Long id) {
        Product product = productMapper.selectById(id);
        if(product.getProductStatus() == 0){
            product.setProductStatus(1);
        }else {
            product.setProductStatus(0);
        }
        productMapper.updateById(product);
    }
    //成功：1， 失败：0
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int minusStock(Long productId, Integer num) {
        int result = 0;
        //1、查询商品库存
        Product product = productMapper.selectById(productId);
        //2、判断库存是否充足
        if(product.getStock() > 0){
            //3、减库存
            // 乐观锁更新库存
            result = productMapper.decreaseStockForVersion(productId,num, product.getVersion());
        }
        return result;
    }

    //成功：1， 失败：0
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int restoreStock(Long productId, Integer num) {
        int result = 0;
        //1、查询商品库存
        Product product = productMapper.selectById(productId);
        //2、判断库存是否充足
        if(product.getStock() > 0){
            //3、加库存
            // 乐观锁更新库存
            result = productMapper.increaseStockForVersion(productId,num, product.getVersion());
        }
        return result;
    }
    @Override
    public Boolean sell(Long productId,Integer num){
        int retryCount = 0;
        int update = 0;
        // 获取库存
        Product goods = productMapper.selectById(productId);
        if (goods.getStock() > 0) {
            // 乐观锁更新库存
            // 更新失败，说明其他线程已经修改过数据，本次扣减库存失败，可以重试一定次数或者返回
            // 最多重试3次
            while(retryCount < 3 && update == 0){
                update = this.minusStock(productId,num);
                retryCount++;
            }
            if(update == 0){
                log.error(productId+"库存不足");
                return false;
            }
            return true;
        }
        // 失败返回
        return false;
    }
    public Boolean restore(Long productId,Integer num){
        int retryCount = 0;
        int update = 0;
        // 获取库存
        Product goods = productMapper.selectById(productId);
        if (goods.getStock() > 0) {
            // 乐观锁更新库存
            // 更新失败，说明其他线程已经修改过数据，本次加减库存失败，可以重试一定次数或者返回
            // 最多重试3次
            while(retryCount < 3 && update == 0){
                update = this.restoreStock(productId,num);
                retryCount++;
            }
            if(update == 0){
                log.error(productId+"恢复失败");
                return  false;
            }
            return true;
        }
        return false;
    }



}
