package com.xxx.service.impl;

import com.alipay.api.AlipayApiException;
import com.xxx.entity.AlipayBean;
import com.xxx.service.PayService;
import com.xxx.utils.AlipayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: chen
 * @Date: 2021/3/23 5:52 PM
 * @Version 1.0
 */
@Service("alipayOrderService")
public class PayServiceImpl implements PayService {

    @Autowired
    private AlipayUtil alipayUtil;
    @Override
    public String aliPay(AlipayBean alipayBean) throws AlipayApiException {
        return  alipayUtil.connect(alipayBean);
    }
}
