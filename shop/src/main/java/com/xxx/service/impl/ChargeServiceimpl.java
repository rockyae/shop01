package com.xxx.service.impl;

import com.alipay.api.AlipayApiException;
import com.xxx.entity.AlipayBean;
import com.xxx.entity.request.ChargeRequest;
import com.xxx.enums.ApiEnum;
import com.xxx.exception.GlobalException;
import com.xxx.service.ChargeService;
import com.xxx.service.PayService;
import com.xxx.utils.IdWorker;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChargeServiceimpl implements ChargeService {


    @Autowired
    private PayService payService;
    @Override
    public String   pay(String amount) throws AlipayApiException {
        IdWorker idWorker = new IdWorker(1,1,1);
        Long l = idWorker.nextId();
        String regx = "^([1-9]\\d{0,9}|0)([.]?|(\\.\\d{1,2})?)$";
        if(!amount.matches(regx)){
            throw new GlobalException(ApiEnum.ERROR);
        }
        String res = payService.aliPay(new AlipayBean().setOut_trade_no(l.toString()).
                setTotal_amount(amount)
                .setSubject("充值"));
        return res;
    }
}
