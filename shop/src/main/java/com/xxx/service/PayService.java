package com.xxx.service;

import com.alipay.api.AlipayApiException;
import com.xxx.entity.AlipayBean;

/**
 * @Author: chen
 * @Date: 2021/3/23 5:52 PM
 * @Version 1.0
 */
public interface PayService {

    /*支付宝支付接口*/
    String aliPay(AlipayBean alipayBean) throws AlipayApiException;

}
