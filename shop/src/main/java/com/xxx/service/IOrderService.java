package com.xxx.service;

import com.alipay.api.AlipayApiException;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.entity.OrderEvaluate;
import com.xxx.entity.UserOrder;
import com.xxx.entity.request.OrderRequest;
import com.xxx.entity.response.OrderResponse;
import com.xxx.enums.OrderEnum;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chen
 * @since 2021-03-23
 */
public interface IOrderService extends IService<UserOrder> {
    Long getOrder(List<OrderRequest> orderRequest);

    OrderResponse orderDesc(String id);

    List<OrderResponse> listOrder(Integer orderStatus);

    String pay(String id,String addressId) throws AlipayApiException;

    void changeOrder(OrderEnum c, String out_trade_no, String addressId);

    void clearOrder() throws AlipayApiException;

    void evaluate(String id, OrderEvaluate orderEvaluate);

    Page<OrderResponse> listOrderAll(OrderRequest orderRequest);

    BigDecimal selectSumToBuy(Date startTime, Date endTime);

    Integer getOrderSum(int i);

    Integer getOrderSumPerDay(Date startTime,Date endTime);

    Boolean tryMinusStock(List<OrderRequest> orderRequest);

}
