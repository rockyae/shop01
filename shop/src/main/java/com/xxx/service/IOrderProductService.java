package com.xxx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.entity.OrderProduct;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chen
 * @since 2021-03-23
 */
public interface IOrderProductService extends IService<OrderProduct> {

}
