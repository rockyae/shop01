package com.xxx.service;

import com.alipay.api.AlipayApiException;
import com.xxx.entity.request.ChargeRequest;

public interface ChargeService {


    String pay(String amount) throws AlipayApiException;

}
