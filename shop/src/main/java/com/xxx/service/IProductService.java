package com.xxx.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.entity.Product;
import com.xxx.entity.request.ProductRequest;
import com.xxx.entity.response.ProductResponse;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author chen
 * @since 2021-03-18
 */
public interface IProductService extends IService<Product> {

    /**
     * 查询商品列表
     * @param request 分页和其他
     * @return
     */
    Page<ProductResponse> findListForPage(ProductRequest request);

    /**
     * 上架或者下架商品
     * @param id
     */
    void upOrDown(Long id);



    @Transactional(rollbackFor = Exception.class)
    int minusStock(Long productId, Integer num);

    Boolean sell(Long productId,Integer num);

    int restoreStock(Long productId, Integer num);

    Boolean restore(Long productId,Integer num);
}
