package com.xxx.service;

import com.alipay.api.AlipayApiException;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxx.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxx.entity.request.UserRequest;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chen
 * @since 2021-03-19
 */
public interface IUserService extends IService<User> {

    void register(UserRequest request);

    Map<String, Object> login(UserRequest request);

    Page<User> findListForPage(UserRequest request);
}
