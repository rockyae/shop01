package com.xxx.service;

import com.xxx.entity.Address;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地址表 服务类
 * </p>
 *
 * @author chen
 * @since 2021-03-23
 */
public interface IAddressService extends IService<Address> {

    void changeDefault(String id);

    void add(Address address);

    Address queryAddress(long id);

    void updateAddress(Address address);
}


