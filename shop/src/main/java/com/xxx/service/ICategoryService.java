package com.xxx.service;

import com.xxx.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chen
 * @since 2021-03-22
 */
public interface ICategoryService extends IService<Category> {

}
