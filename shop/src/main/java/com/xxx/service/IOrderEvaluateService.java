package com.xxx.service;

import com.xxx.entity.OrderEvaluate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chen
 * @since 2021-03-26
 */
public interface IOrderEvaluateService extends IService<OrderEvaluate> {

}
