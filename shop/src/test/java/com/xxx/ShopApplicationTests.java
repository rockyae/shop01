package com.xxx;

import com.alipay.api.AlipayApiException;
import com.xxx.job.OrderJob;
import com.xxx.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ShopApplicationTests {

    @Autowired
    private ProductServiceImpl productService;

    @Autowired
    private OrderJob orderJob;
    @Test
    void test() throws AlipayApiException {
        orderJob.clearOrder();
    }

}
