package com.xxx;



public class Test {
    // 位数，默认是8位
    private final static long w = 100000000;

    @org.junit.jupiter.api.Test
    public  void createID() throws InterruptedException {
            long r = 0;
            synchronized (this) {
                //得到一个8位数
                r = (long) (Math.random() * w);
            }
        for (int i = 0; i < 20; i++) {
            System.out.println(String.valueOf(System.currentTimeMillis()%w + r));
            Thread.sleep(5000);
        }
        }
}
