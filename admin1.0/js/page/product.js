//var perfix = "http://150.158.199.52:8080"
var perfix = "http://127.0.0.1:8080"
function products() {
	this.product = []
}

function search() {
	this.productName = null,
		this.productTitle = null,
		this.startTime = null,
		this.endTime = null,
		this.page = 1,
		this.size = 10
}


var search = new search();
//查询ajax
function doPost(search) {
	//发送接口请求
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + "/product/list",
		data: JSON.stringify(search),
		type: 'post',
		contentType: 'application/json',
		success: function(data) {
			if (data.code == 10006) {
				localStorage.clear();
				$(window).attr('location',"http://127.0.0.1:8848/item-shop/page/login.html");
			}
			var lists = data.data.records;
			//--- 没查询到数据 --- 
			if (lists.length == 0) {
				$("#tableList").append($(`
					<tr>
						<td align="center" colspan="10">什么都没有</td>
					</tr>
				`))
			}
			// --- 渲染数据到列表上 --- 
			for (var i = 0; i < lists.length; i++) {
				var caozuo = "上架商品";
				if (lists[i].productStatus == 0) {
					lists[i].productStatus = "已上架";
					caozuo = "下架商品";
				} else {
					lists[i].productStatus = "已下架"
				}
				$("tbody").append($(
					`
					<tr>
						<td>${lists[i].id}</td>
						<td>${lists[i].productName}</td>
						<td>${lists[i].categoryName}</td>
						<td>${lists[i].productTitle}</td>
						<td>${lists[i].productDesc}</td>
						<td>${lists[i].stock}</td>
						<td>¥${lists[i].productPrice}</td>
						<td><img width="150" height="150" src="${lists[i].productImg}" class="img-thumbnail" alt="..."></td>
						<td><span class="label label-success">${lists[i].productStatus}</span></td>
						<td>${lists[i].updateTime}</td>
						<td>
							<span class="label label-info xiajia" style="margin-top:30%;display:block;">${caozuo}</span><input type="hidden" id="idCat" value="${lists[i].id}" />
							<span class="label label-primary modify" style="margin-top:30%;display:block;" href="#ModifyModel" data-toggle="modal">修改商品</span>
							<span class="label label-danger delete"style="margin-top:30%;display:block;">删除商品</span>
						</td>
					</tr>
				`
				))
				$(".xiajia").unbind();
				$(".xiajia").on('click', function() {
					if(confirm("确定需要"+$(this).text()+"？")){
						var id = $(this).parent("td").siblings()[0].childNodes[0].data;
						var model = {
							"id":id
						}
						upOrDown(model);
					}
				})
			}
			$(".modify").on('click', function(){
				$("#productId").val($(this).parent("td").siblings()[0].childNodes[0].data);
				$("#productNameModify").val($(this).parent("td").siblings()[1].childNodes[0].data);
				$("#category").find("option[text="+"'"+$(this).parent("td").siblings()[2].childNodes[0].data+"'"+"]").attr("selected",true);
				$("#productTitleModify").val($(this).parent("td").siblings()[3].childNodes[0].data);
				$("#productDescModify").val($(this).parent("td").siblings()[4].childNodes[0].data);
				$("#productStockModify").val($(this).parent("td").siblings()[5].childNodes[0].data);
				$("#productPriceModify").val($(this).parent("td").siblings()[6].childNodes[0].data);
				$("#productImage").attr('src',$(this).parent("td").siblings()[7].childNodes[0].currentSrc);
			})
			$(".delete").on('click', function(){
				var productId = $(this).parent("td").siblings()[0].childNodes[0].data;
				if(confirm("确定需要删除吗?（只有下架商品才可删除）")){
					$.ajax({
						headers: {
							'Authorization': localStorage.getItem("token")
						},
						url: perfix + "/product/delete",
						data: JSON.stringify(productId),
						type: 'post',
						contentType: 'application/json',
						success: function(data) {
							if(data.code != 200){
								alert(data.message);
								return;
							}
							alert("删除成功");
							location.reload();
						}
					})
				}
			})
			// --- 分页控件 ---

			$("#pages").children().remove();

			var pages = data.data.pages;
			//当前页数
			var current = data.data.current;
			if (current == 1) {
				$("#pages").append($(`
					<li><a class="btn btn-link  disabled" href="#" >&laquo;</a></li>
				`))
			} else {
				$("#pages").append($(`
					<li><a class="btn btn-link lastPage" href="#" >&laquo;</a></li>
				`))
				$(".lastPage").on('click', function() {
					search.page = current - 1;
					$("tbody").children().remove();
					doPost(search);
				})
			}
			
			for (var i = 0; i < pages; i++) {
				if (current === i + 1) {
					$("#pages").append($(`
						<li class="active"><a class="pagex" href="#" >${i + 1}</a></li>
					`))
				} else {
					$("#pages").append($(`
						<li class="everyPage" ><a class="pagex" >${i+1}</a></li>
					`))
				}
				$(".everyPage").unbind()
				$(".everyPage").on('click', function() {
					console.log($(this).text());
					search.page = $(this).text();
					$("tbody").children().remove();
					doPost(search);
				})
			}

			if (current == pages) {
				$("#pages").append($(`
					<li><a class="btn btn-link  disabled" href="#" >&raquo;</a></li>
				`))
			} else {
				$("#pages").append($(`
					<li class="nextPage"><a class="btn btn-link"  >&raquo;</a></li>
				`))
				$(".nextPage").on('click', function() {
					search.page = current + 1;
					$("tbody").children().remove();
					doPost(search);
				})
			}
			// --- 分页控件 ---
		}
	})
}
//初始化列表
doPost(search);

//点击查询按钮
$("#searchList").on('click', function() {
	search.productName = $("#productName").val();
	search.productTitle = $("#productTitle").val();
	search.startTime = $("#startTime").val();
	search.endTime = $("#endTime").val();
	$("tbody").children().remove();
	doPost(search);
})

//上架或者下架
function upOrDown(product) {
	// product.productStatus = null;
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + "/product/upOrDown",
		data: JSON.stringify(product),
		type: 'post',
		contentType: 'application/json',
		success: function(data) {
			if(data.code != 200){
				alert("服务器错误");
				return;
			}
			alert("修改成功");
			location.reload();
		}
	})
}

//添加表单实体
function addForm() {
	this.productName = null,
		this.productTitle = null,
		this.productDesc = null,
		this.productImg = null,
		this.productPrice = null,
		this.productStatus = null,
		this.cateId = null
}

var addForm = new addForm();
//点击新增商品提交按钮
$("#productAdd").on('click', function() {
	var formData = new FormData;
	formData.append("file", $("#productImgAdd")[0].files[0]);

	//上传图片
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + '/product/doUpLoad',
		type: 'post',
		cache: false, //上传文件不需要缓存
		data: formData,
		processData: false, // 告诉jQuery不要去处理发送的数据
		contentType: false, // 告诉jQuery不要去设置Content-Type请求头
		success: function(data) {
			addForm.cateId = $("#category  option:selected").val();
			addForm.productName = $("#productNameAdd").val();
			addForm.productTitle = $("#productTitleAdd").val();
			addForm.productDesc = $("#productDescAdd").val();
			//后端返回的云端图片地址
			addForm.productImg = data.data;
			addForm.productPrice = $("#productPriceAdd").val();
			addForm.productStock = $("#productStock").val();
			if ($("#productStatus").prop('checked')) {
				addForm.productStatus = 0;
			} else {
				addForm.productStatus = 1;
			}
			doAdd(addForm);
		}
	})
})
$("#productModify").on('click', function(){
	var modifyForm = {
		id: $("#productId").val(),
		productName: $("#productNameModify").val(),
		productTitle: $("#productTitleModify").val(),
		productDesc: $("#productDescModify").val(),
		productImg: $("#productImage").val(),
		stock: parseInt($("#productStockModify").val()),
		productPrice: $("#productPriceModify").val(),
		productStatus: $("#productStatusM").prop('checked') ? 0 : 1,
		cateId: $("#category2 option:selected").val()
		}
	doModify(modifyForm);
})



//新增商品
function doAdd(addForm) {
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + "/product/add",
		data: JSON.stringify(addForm),
		type: 'post',
		contentType: 'application/json',
		success: function(data) {
			alert("新增成功");
			location.reload();
		}
	})
}
//修改商品
function doModify(modifyForm){
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + "/product/modify",
		data: JSON.stringify(modifyForm),
		type: 'post',
		contentType: 'application/json',
		success: function(data) {
			alert("修改成功");
			location.reload();
		}
	})
}
//分类初始化
$.ajax({
	headers: {
		'Authorization': localStorage.getItem("token")
	},
	url: perfix + "/category/list",
	data: '',
	type: 'post',
	contentType: 'application/json',
	success: function(data) {
		for(var i =0 ;i<data.data.length; i ++ ){
			$("select").append($(
						`
						<option value="${data.data[i].id}">${data.data[i].categoryName}</option>
					`
					))
		}
		
	}
})
//新增分类
$("#cateBtn").on('click',function(){
	
	var model = {
		"categoryName":$("#categoryNameAdd").val()
	}
	
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + "/category/add",
		data: JSON.stringify(model),
		type: 'post',
		contentType: 'application/json',
		success: function(data) {
			if(data.code == 10009){
				alert(data.message);
				return;
			}
			alert("新增成功");
			location.reload();
		}
	})
})