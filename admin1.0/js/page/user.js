//var perfix = "http://150.158.199.52:8080"
var perfix = "http://127.0.0.1:8080"
//用户对象
function user() {
	this.users = [{
		"id": null,
		"username": null,
		"createTime": null,
		"status": null,
	}]

}
//查询实体
function search() {
	this.page = 1;
	this.size = 10;
	this.username = null;
}

var search = new search();
//加载数据
function doPost(search) {
	var model = {
		"page": search.page,
		"size": search.size,
		"username": search.username
	}
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + '/user/list',
		data: JSON.stringify(model),
		type: 'post',
		contentType: 'application/json',
		success: function(data) {
			if (data.code == 10006) {
				localStorage.clear();
				$(window).attr('location',"http://127.0.0.1:8848/item-shop/page/login.html");
			}
			var userlist = data.data.records;
			for (var i = 0; i < userlist.length; i++) {
				var status = null;
				if(userlist[i].userStatus == 0){
					status = "冻结";
					$("#usertable").append($(
						`
						<tr>
							<td>${userlist[i].id}</td>
							<td>${userlist[i].username}</td>
							<td>${userlist[i].createTime}</td>
							<td><span class="label label-danger dongjie">${status}</span></td>
						</tr> 
						`
					))
				}else{
					status = "激活";
					$("#usertable").append($(
						`
						<tr>
							<td>${userlist[i].id}</td>
							<td>${userlist[i].username}</td>
							<td>${userlist[i].createTime}</td>
							<td><span class="label label-info dongjie">${status}</span></td>
						</tr> 
						`
					))
				}
				
				
				
				$(".dongjie").unbind();
				$(".dongjie").on('click',function(){
					if(confirm("确定需要"+$(this).text()+"？")){
						var id = $(this).parent("td").siblings()[0].childNodes[0].data;
						var model = {
							"id":id
						}
						update(model);
					}
				})
			}
			// --- 分页控件 ---
			
			$("#pages").children().remove();
			//总页数
			var pages = data.data.pages;
			//当前页数
			var current = data.data.current;
			if (current == 1) {
				$("#pages").append($(`
					<li><a class="btn btn-link  disabled" href="#" >&laquo;</a></li>
				`))
			} else {
				$("#pages").append($(`
					<li><a class="btn btn-link lastPage" href="#" >&laquo;</a></li>
				`))
				$(".lastPage").on('click', function() {
					search.page = current - 1;
					$("tbody").children().remove();
					doPost(search);
				})
			}
			
			for (var i = 0; i < pages; i++) {
				if (current === i + 1) {
					$("#pages").append($(`
						<li class="active"><a class="pagex" href="#" >${i + 1}</a></li>
					`))
				} else {
					$("#pages").append($(`
						<li class="everyPage" ><a class="pagex" >${i+1}</a></li>
					`))
				}
				$(".everyPage").unbind()
				$(".everyPage").on('click', function() {
					console.log($(this).text());
					search.page = $(this).text();
					$("tbody").children().remove();
					doPost(search);
				})
			}
			
			if (current == pages) {
				$("#pages").append($(`
					<li><a class="btn btn-link  disabled" href="#" >&raquo;</a></li>
				`))
			} else {
				$("#pages").append($(`
					<li class="nextPage"><a class="btn btn-link"  >&raquo;</a></li>
				`))
				$(".nextPage").on('click', function() {
					search.page = current + 1;
					$("tbody").children().remove();
					doPost(search);	
				})
			}
			// --- 分页控件 ---
		}

	})
}
// 初始化数据
doPost(search);

// 点击查询按钮
$("#searchBtn").on('click',function(){
	search.username = $("#username").val();
	$("tbody").children().remove();
	doPost(search);
})

//冻结解冻用户
function update(user){
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + '/user/update',
		type: 'post',
		data:JSON.stringify(user),
		contentType: 'application/json',
		success:function(data){
			location.reload();
		}
	})
}

//导出用户
$("#exportUser").on("click",function(){
	window.location.href = perfix + '/user/export';
})

//导入用户
$("#importUser").on('click',function(){
	$("#upload").trigger("click");
	$("#upload").on('change',function(){
		var formData = new FormData;
		formData.append("file", $("#upload")[0].files[0]);
		//上传图片
		$.ajax({
			headers: {
				'Authorization': localStorage.getItem("token")
			},
			url: perfix + '/user/import',
			type: 'post',
			cache: false, //上传文件不需要缓存
			data: formData,
			processData: false, // 告诉jQuery不要去处理发送的数据
			contentType: false, // 告诉jQuery不要去设置Content-Type请求头
			success: function(data) {
				location.reload();
			}
		})
	})
})
