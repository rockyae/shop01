$("#address").on("click",function(){
	$(window).attr('location',"./addresslist.html");
})
//点击返回
$("#back").on("click",function(){
	$(window).attr('location',"../index.html");
})
// --------- 用户数据 ---------
var userId = localStorage.getItem("userId");
var token = localStorage.getItem("token");
console.log(userId);
if(token == null ){
	$(".bg-login").text("登录/注册")
	$(".bg-login").on('click',function(){
		$(window).attr('location',"./login.html");
	})
}else{
	var username = localStorage.getItem("username");
	var pic = localStorage.getItem("userPic");
	if(pic == null){
		$("#pic").attr('src',"../image/bg-pic.png");
	}else{
		$("#pic").attr('src',pic);
	}
	$(".bg-login").text(username)	
}
$.ajax({
	headers: {
		'Authorization': localStorage.getItem("token")
	},
	url: perfix + '/user/queryAll',
	type: 'get',
	success: function(data) {
		var balance = data.data.balance;
		var vipLevel = data.data.vipLevel;
		if(vipLevel == 0){
			$('#vipLevel').text("普通会员");
			$('.vipLevel').append('<i style="display: inline-block;font-size:30px;">(无优惠)</i>')
		}else if(vipLevel == 1){
			$('#vipLevel').text("黄金会员");
			$('.vipLevel').append('<i style="display: inline-block;font-size:30px;">(打九折)</i>')
		}else{
			$('#vipLevel').text("钻石会员");
			$('.vipLevel').append('<i style="display: inline-block;font-size:30px;">(打七折)</i>')
		}
		if(balance > 0){
			$('#balance').text('￥'+balance+'');
		}else{
			$('#balance').text('￥'+0.00+'');
		}
	}
})



// ---------- 上传头像 ----------
$("#pic").on('click',function(){
	$("#upload").click();
})

//上传头像
$("#upload").on('change',function(){
	var formData = new FormData;
	formData.append("file", $("#upload")[0].files[0]);
	
	//上传图片
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + '/product/doUpLoad',
		type: 'post',
		cache: false, //上传文件不需要缓存
		data: formData,
		processData: false, // 告诉jQuery不要去处理发送的数据
		contentType: false, // 告诉jQuery不要去设置Content-Type请求头
		success: function(data) {
			var user = {
				"id":localStorage.getItem("userId"),
				"userImg":data.data
			}
			update(user);
			localStorage.setItem("userPic",data.data);
		}
	})
})

$("#myOrder1").on('click',function(){
	$(window).attr('location',"./order1.html");
})
$("#myOrder2").on('click',function(){
	$(window).attr('location',"./order2.html");
})
$("#myOrder3").on('click',function(){
	$(window).attr('location',"./order3.html");
})


$("#myOrder").on('click',function(){
	$(window).attr('location',"./order-all.html");
})


//调用用户更新接口
function update(user){
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + '/user/update',
		type: 'post',
		data:JSON.stringify(user),
		contentType: 'application/json',
		success:function(data){
			location.reload();
		}
	})
}

//------- 退出登录  -------
$("#outLogin").on('click',function(){
	if(localStorage.getItem("token") == null){
		return;
	}
	localStorage.removeItem("username");
	localStorage.removeItem("userPic");
	localStorage.removeItem("userId");
	localStorage.removeItem("token");
	location.reload();
	$(window).attr('location',"./login.html");
})
//------- 退出登录  -------
$("#saveCharge").on('click',function(){
	var amount = $("#amount").val();
	if(amount <= 0){
		alert("充值金额必须大于0！");
		return;
	}
	$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + '/order/charge/'+amount,
		type: 'get',
		contentType: 'application/json',
		success:function(data){
						if (data.code == 10006) {
							localStorage.clear();
							$(window).attr('location', "login.html");
						}
						if(data.code == 10010){
							alert(data.message);
							return;
						}
						//将接口返回的Form表单显示到页面
						document.querySelector('body').innerHTML = data.data;
						//调用submit 方法
						document.forms[0].submit()
					}
	})
})






