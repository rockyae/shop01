$("#back").on("click", function() {
	$(window).attr('location', "./user.html");
})
var order = new Array();
$.ajax({
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		url: perfix + '/order/list?orderStatus=2',
		type: 'get',
		contentType: 'application/json',
		success:function(data){
			if (data.code == 10006) {
				localStorage.clear();
				$(window).attr('location', "./login.html");
			}
			for (var i = 0; i < data.data.length; i++) {
				order[i] = {
					"id": data.data[i].id,
					"productDesc": [],
					"priceAll": data.data[i].orderPrice,
					"orderStatus": data.data[i].orderStatus
			
				}
				for (var j = 0; j < data.data[i].products.length; j++) {
					order[i].productDesc[j] = {
						"productName": data.data[i].products[j].productName,
						"productPrice": data.data[i].products[j].productPrice,
						"productNum": data.data[i].products[j].productNum,
						"productImg": data.data[i].products[j].productImg,
					}
				}
			}
			//如果用户还没有订单
			if(order.length == 0){
				$("#box").append('<div class="box">您还没有任何订单，快去选购吧^_^</div>');
			}
			for (var i = 0; i < order.length; i++) {
					var domid = Math.ceil(Math.random() * 10000);
					$("#box").append('<div class="box"  id= "' + domid + '">');
					for (var j = 0; j < order[i].productDesc.length; j++) {
						var id = '#' + domid
						$(id).append($(
							`
						<div class="prd-list">
							<div class="prd-img">
								<img class="imgs" class="img-thumbnail" src="${order[i].productDesc[j].productImg}" />
							</div>
							<div class="prd-desc">
								<p>${order[i].productDesc[j].productName}</p>
							</div>
							<div class="prd-price">
								<p>¥${order[i].productDesc[j].productPrice}</p>
								<p>数量：x<span>${order[i].productDesc[j].productNum}</span></p>
							</div>
						</div>
						<input type="hidden" value="${order[i].id}"/>
						`
						))
					}
					$(id).append($(
						`<div class="prd-all-price">
								总价¥${order[i].priceAll}
							</div>
							<button class="btn btn-info btn-do">确认收货</button>
							<button class="btn btn-danger btn-del">删除</button>`))
			}
		//点击查询详情
		$(".prd-list").on('click', function() {
			$(window).attr('location', "./orderdesc.html?id=" + $(this).next().val());
		})
	}
		})
