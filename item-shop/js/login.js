$("#register").click(function(){
	$(window).attr('location',"../page/register.html");
});
// ---------- 获取验证码 ----------
function getVerificationCode(){
	$.ajax({
		url: perfix + '/user/verificationCode',
		data: '',
		type: 'post',
		success:function(data){
			$("#vCode").attr('src',data.data.encoder);
			$("#uuid").val(data.data.key);
		}
	})
}
getVerificationCode();
//点击刷新验证码
$("#vCode").on('click',function(){
	getVerificationCode();
})
// ---------- 获取验证码 ----------
// ------------ 登录 ------------
$("#loginBtn").on('click',function(){
	var username = $("#username").val();
	var password = $("#password").val();
	var verificationCode = $("#verificationCode").val();
	var uuid = $("#uuid").val();
	
	var model = {
		"username":username,
		"password":password,
		"verificationCode":verificationCode,
		"uuid":uuid
	}
	$.ajax({
		url: perfix+'/user/login',
		data: JSON.stringify(model),
		type: 'post',
		contentType:'application/json',
		success:function(data){
			if(data.code != 200){
				alert(data.message);
				return;
			}
			
			//保存token和user
			localStorage.setItem("userId",data.data.user.id);
			localStorage.setItem("username",data.data.user.username);
			localStorage.setItem("userPic",data.data.user.userImg);
			localStorage.setItem("token",data.data.token);
			alert("登录成功")
			if(data.data.flag != 'admin'){
				$(window).attr('location',"../index.html");
			}else{
				$(window).attr('location',"http://127.0.0.1:8848/admin1.0/index.html");
			}
		}
	})
	
})

