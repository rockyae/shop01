function addressList(){
	this.addresses = []
}

var addressList = new addressList();
var currentId;

$.ajax({
	url: perfix + "/address/list",
	data: '',
	type: 'post',
	headers: {
		'Authorization': localStorage.getItem("token")
	},
	contentType: 'application/json',
	success: function(data) {
		if (data.code == 10006) {
			localStorage.clear();
			$(window).attr('location', "login.html");
		}
		for(var i = 0;i<data.data.length;i++){
			addressList.addresses[i] = {
				"id":data.data[i].id,
				"nickname":data.data[i].nickname,
				"telephone":data.data[i].telephone,
				"addressDesc":data.data[i].addressDesc,
				"addressStatus":data.data[i].isDefault
			}
		}
		//数据渲染
		for(var i =0 ;i<addressList.addresses.length; i++){
			if(addressList.addresses[i].addressStatus == 0){
				$("#addressListBox").append($(
					`
							<div class="box2">
								<div class="address">	
									<p><span>${addressList.addresses[i].nickname}</span> <span>${addressList.addresses[i].telephone}</span> <button class="btn btn-danger btn-def">默认</button></p>
									<p class="address-desc">${addressList.addresses[i].addressDesc}</p>
								</div>
								<input value="${addressList.addresses[i].id}" type="radio" name="cheakRadios">
							</div>
				`
				))
			}else{
				$("#addressListBox").append($(
					`
							<div class="box2">
								<div class="address">	
									<p><span>${addressList.addresses[i].nickname}</span> <span>${addressList.addresses[i].telephone}</span> </p>
									<p class="address-desc">${addressList.addresses[i].addressDesc}</p>
								</div>
								<input value="${addressList.addresses[i].id}" type="radio" name="cheakRadios">
							</div>
				`
				))
			}
		}
		//点击查询详情
		$(".address").on('click', function() {
			console.log('点击');
			$(window).attr('location', "./addressDesc.html?id=" + $(this).next().val());
		})
		
	}
})

$("#addAddress").on("click", function() {
	$(window).attr('location', "./addressAdd.html");
})
$("#back").on("click", function() {
	$(window).attr('location', "./user.html");
})
$("#switchDefaultAddress").on("click", function() {
	$("input").each(function(){
		if(this.checked){
			currentId = this.value;
		};
	});
	$.ajax({
		url: perfix + "/address/changeDefault/"+currentId,
		data: '',
		type: 'post',
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		success: function(data) {
			if (data.code == 10006) {
				localStorage.clear();
				$(window).attr('location', "login.html");
			}
			$(window).attr('location', "addresslist.html");
		}
	})
})


