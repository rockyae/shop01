$("#back").on("click", function() {
	$(window).attr('location', "./order-all.html");
})

// --------- 获取url上的值 ---------
function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
	var r = window.location.search.substr(1).match(reg); //匹配目标参数
	if (r != null) return unescape(r[2]);
	return null; //返回参数值
}
// --------- 获取url上的值 ---------

//返回值对象
function orderdesc() {
	this.id = 1;
	this.totalPrice = null;
	this.address = {};
	this.orderdesc = [];
}
var order = new orderdesc();
// -------------ajax初始化数据-------------
$.ajax({
	url: perfix + "/order/orderDesc/" + getUrlParam("id"),
	data: '',
	type: 'post',
	headers: {
		'Authorization': localStorage.getItem("token")
	},
	contentType: 'application/json',
	success: function(data) {
		if (data.code == 10006) {
			localStorage.clear();
			$(window).attr('location', "login.html");
		}
		// 数据加载1
		order.id = data.data.id;
		order.totalPrice = data.data.orderPrice
		// 数据加载2
		for (var i = 0; i < data.data.products.length; i++) {
			order.orderdesc[i] = {
				"id": data.data.products[i].id,
				"prodName": data.data.products[i].productName,
				"img": data.data.products[i].productImg,
				"price": data.data.products[i].productPrice,
				"num": data.data.products[i].productNum
			}
		}
		//数据加载3
		if (data.data.address == null) {
			order.address = {
				"id": "",
				"relName": "请先添加地址",
				"phone": "",
				"addressName": "",
			}
		} else {
			order.address = {
				"id": data.data.address.id,
				"relName": data.data.address.nickname,
				"phone": data.data.address.telephone,
				"addressName": data.data.address.addressDesc
			}
		}

		//数据渲染
		for (var i = 0; i < order.orderdesc.length; i++) {
			console.log(123)
			$("#box").append(
				'<div class="prd-list"><div class="prd-img"><img class="imgs" class="img-thumbnail" src="' + order.orderdesc[i]
				.img + '" /></div><div class="prd-desc"><p>' + order.orderdesc[i].prodName +
				'</p></div><div class="prd-price"><p>¥' + order.orderdesc[i].price + '</p><p>数量：x<span>' + order.orderdesc[i].num +
				'</span></p></div></div>')
		}
		if (data.data.orderStatus == 0) {
			$("#box").append('<div class="prd-all-price"> 总价¥ ' + order.totalPrice +
				'</div><button class="btn btn-info btn-do" id="doPost" disabled>已失效<input type="hidden" value="' + order.id +
				'" id="orderId" /></button><button id="deleteOrder"  class="btn btn-danger btn-del">删除</button>');
		}
		if (data.data.orderStatus == 1) {
			$("#box").append('<div class="prd-all-price"> 总价¥ ' + order.totalPrice +
				'</div><button class="btn btn-info btn-do" id="pay">付款<input type="hidden" value="' + order.id +
				'" id="orderId" /></button><button id="deleteOrder"  class="btn btn-danger btn-del">删除</button>');
			$('#pay').on('click', function() {
				$.ajax({
					url: perfix + "/order/pay/" + getUrlParam("id")+"/"+data.data.address.id,	
					data: '',
					type: 'post',
					headers: {
						'Authorization': localStorage.getItem("token")
					},
					contentType: 'application/json',
					success: function(data) {
						if (data.code == 10006) {
							localStorage.clear();
							$(window).attr('location', "login.html");
						}
						if(data.data == "success"){
							alert("余额充足，支付成功!")
							$(window).attr('location', "order-all.html");
						}
						//将接口返回的Form表单显示到页面
						document.querySelector('body').innerHTML = data.data;
						//调用submit 方法
						document.forms[0].submit()

					}
				})
			})
		}
		if (data.data.orderStatus == 2) {
			$("#box").append('<div class="prd-all-price"> 总价¥ ' + order.totalPrice +
				'</div><button class="btn btn-info btn-do" id="doGoods">收货<input type="hidden" value="' + order.id +
				'" id="orderId" /></button><button id="deleteOrder"  class="btn btn-danger btn-del">删除</button>');
			$('#doGoods').on('click', function() {
				$.ajax({
					url: perfix + "/order/doGoods/" + getUrlParam("id"),
					data: '',
					type: 'post',
					headers: {
						'Authorization': localStorage.getItem("token")
					},
					contentType: 'application/json',
					success: function(data) {
						if (data.code == 10006) {
							localStorage.clear();
							$(window).attr('location', "login.html");
						}
						location.reload();
					}
				})
			})	
		}
		if (data.data.orderStatus == 3) {
			$("#box").append($(
				`
				<div class="prd-all-price">
					总价¥${order.totalPrice}
				</div>
				
				<button class="btn btn-primary  btn-do" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
				    评价
				  </button>
				
				<button id="deleteOrder" class="btn btn-danger btn-del">删除</button>
				
				<div class="collapse" style="margin-top: 20px;" id="collapseExample">
				  <div class="card card-body">
				    <input type="text"  style="font-size: 36px;height: 80px;"  class="form-control" placeholder="你的评价。。。">
					<button id="evaluate" class="btn btn-success btn-do btn-pinlun">提交</button>
				  </div>
				</div>
				
		`
			));
			$('#evaluate').on('click', function() {
				
				var model = {
					"evaluateDesc":$(this).prev().val()
				}
				$.ajax({
					url: perfix + "/order/evaluate/" + getUrlParam("id"),
					data: JSON.stringify(model),
					type: 'post',
					headers: {
						'Authorization': localStorage.getItem("token")
					},
					contentType: 'application/json',
					success: function(data) {
						if (data.code == 10006) {
							localStorage.clear();
							$(window).attr('location', "login.html");
						}
						
						location.reload();
					}
				})
			})
		}
		if (data.data.orderStatus == 4) {
			$("#box").append('<div class="prd-all-price"> 总价¥ ' + order.totalPrice +
				'</div><button class="btn btn-info btn-do" id="doPost" disabled>已完成<input type="hidden" value="' + order.id +
				'" id="orderId" /></button><button id="deleteOrder"  class="btn btn-danger btn-del">删除</button>');
		}
		
		$("#deleteOrder").on('click',function(){
			$.ajax({
				url: perfix + "/order/delete/" + getUrlParam("id"),
				data: '',
				type: 'post',
				headers: {
					'Authorization': localStorage.getItem("token")
				},
				contentType: 'application/json',
				success: function(data) {
					if (data.code == 10006) {
						localStorage.clear();
						$(window).attr('location', "login.html");
					}
					$(window).attr('location', "./order-all.html");
				}
			})
		})
		


		$("#address").append('<p><span>' + order.address.relName + '</span> <span>' + order.address.phone +
			'</span> </p><p class="address-desc">' + order.address.addressName + '</p>')

	}
})


// -------------ajax-------------

// -------------ajax初始化数据-------------
// order.id = getUrlParam("orderId");
// var model = {
// 		"orderId": order.id
// 	}
// $.ajax({
// 	url: "http://localhost:8080/order/list", //请求的url链接
// 	type: "post", //请求方式
// 	data: JSON.stringify(model), //请求的数据
// 	contentType: "application/json", //请求的数据类型
// 	success: function(data) { 
// 		order.address = {
// 			"id":data.data.address.id,
// 			"relName":data.data.address.relName,
// 			"phone":data.data.address.phone,
// 			"addressName":data.data.address.addressName
// 		}
// 		order.totalPrice = data.data.totalPrice;
// 		for(var i = 0;i<data.data.orderdesc.length;i++){
// 			order.orderdesc.push(data.data.orderdesc[i]);
// 		}

// 	}
// })
// -------------ajax-------------




// -------------ajax提交付款-------------
// $("#doPost").on('click',function(){
// 	$.ajax({
// 		url: "http://localhost:8080/order/buy", //请求的url链接
// 		type: "post", //请求方式
// 		data: JSON.stringify(model), //请求的数据
// 		contentType: "application/json", //请求的数据类型
// 		success: function(data) { 


// 		}
// })

// -------------ajax提交付款-------------
