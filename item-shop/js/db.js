$.ajax({
	url: "http://localhost:8080/data-source-x/list", //请求的url链接
	type: "post", //请求方式
	data: "", //请求的数据
	contentType: "application/json", //请求的数据类型
	success: function(data) { //请求成功时处理的函数  data是后端响应的数据
		var datalist = data.data;
		for (var i = 0; i < datalist.length; i++) {
			$("#dataSource").append('<option id="dataOption" value="' + datalist[i].id + '">' + datalist[i].dbType +
				'</option> ')
				$("#dataSourceAdd").append('<option id="dataOption" value="' + datalist[i].id + '">' + datalist[i].dbType +
					'</option> ')
				
		}

	}
})

$("#search").on('click', function() {
	$("#tablelist").children().remove()
	var dataSource = $("#dataSource  option:selected").val();
	var modelName = $("#modelName").val();
	var author = $("#author").val();
	var model = {
		"author": author,
		"dataId": dataSource,
		"modelName": modelName,
	}
	$.ajax({
		url: "http://localhost:8080/code-generator-x/list", //请求的url链接
		type: "post", //请求方式
		data: JSON.stringify(model), //请求的数据
		contentType: "application/json", //请求的数据类型
		success: function(data) { //请求成功时处理的函数  data是后端响应的数据
			var datalist = data.data.records;
			for (var i = 0; i < datalist.length; i++) {
				$("#tablelist").append('<tr><th>'+datalist[i].dataName+'</th>' + '<td>'+datalist[i].author+'</td>' + '<td>'+datalist[i].packageUrl+'</td>' + '<td>'+datalist[i].tableName+'</td>' + '<td>'+datalist[i].modelName+
					'</td>' + '<td>'+datalist[i].createTime+'</td>' +
					'<td class="td-btn"><button class="btn btn-danger btn-del" >删除</button><button class="btn btn-info">执行</button><input type="hidden" value="'+datalist[i].id+'" /></td>')
					$(".btn-del").unbind();
					$(".btn-del").on('click',function(){
						var id = $(this).next().next().val();
						console.log(id);
						var model = {
							"id": id
							
						}
						$.ajax({
							url: "http://localhost:8080/code-generator-x/delete", //请求的url链接
							type: "post", //请求方式
							data: JSON.stringify(model), //请求的数据
							contentType: "application/json", //请求的数据类型
							success: function(data) { //请求成功时处理的函数  data是后端响应的数据
							window.location.reload();
							}
						})
					})
					
			}
			

		}
	})
})



$("#saveBtn").on('click',function(){
	var dataSource = $("#dataSourceAdd  option:selected").val();
	var modelName = $("#modelName").val();
	var authorX = $("#authorX").val();
	var packageName = $("#packageName").val();
	var tableName = $("#tableName").val();
	var model = {
		"author": authorX,
		"dataId": dataSource,
		"modelName": modelName,
		"packageUrl": packageName,
		"tableName": tableName,
	}
	$.ajax({
		url: "http://localhost:8080/code-generator-x/add", //请求的url链接
		type: "post", //请求方式
		data: JSON.stringify(model), //请求的数据
		contentType: "application/json", //请求的数据类型
		success: function(data) { //请求成功时处理的函数  data是后端响应的数据
		$('#exampleModal').modal('hide')
		
		}
	})
})

