$("#back").on("click", function() {
	$(window).attr('location', "./addresslist.html");
})
var search = location.search;
var id = search.substring(search.indexOf("=") + 1);
				$.ajax(
				{
					url: perfix + "/address/queryAddressDesc/" + id,
					type: 'get',
					headers: {
						'Authorization': localStorage.getItem("token")
					},
					contentType: 'application/json',
					success: function(data) {
						if (data.code == 10006) {
							localStorage.clear();
							$(window).attr('location', "login.html");
						}
						$("#nickname").val(data.data.nickname);
						$("#telephone").val(data.data.telephone);
						$("#addressDesc").val(data.data.addressDesc);
						$("#postalCode").val(data.data.postcode);
					}
				})
$("#commitBtn").on('click', function() {	
	var nickname = $("#nickname").val();
	var telephone = $("#telephone").val();
	var reg =/^1[34578]\d{9}$/;
	var addressDesc = $("#addressDesc").val();
	var postalCode = $("#postalCode").val();
	if(nickname == null || nickname==""){
		alert("请输入必填项！");
		return;
	}
	if(telephone == null || telephone==""){
		alert("请输入必填项！");
		return;
	}
	if(!reg.test(telephone)){
		alert("请输入正确的手机号");
		return;
	}
	if(addressDesc == null || addressDesc==""){
		alert("请输入必填项！");
		return;
	}
	if(postalCode == null || postalCode==""){
		alert("请输入必填项！");
		return;
	}
	if ($("#isDefault").prop('checked')) {
		var isDefault = 0;
	} else {
		var isDefault = 1;
	}
	
	var model = {
		"nickname": nickname,
		"telephone": telephone,
		"addressDesc": addressDesc,
		"postalCode": postalCode,
		"isDefault": isDefault,
		"id": id
	}

	$.ajax({
		url: perfix + "/address/update",
		data: JSON.stringify(model),
		type: 'post',
		headers: {
			'Authorization': localStorage.getItem("token")
		},
		contentType: 'application/json',
		success: function(data) {
			if (data.code == 10006) {
				localStorage.clear();
				$(window).attr('location', "login.html");
			}
			//已存在用户默认地址
			if(data.code == 10008&&isDefault==0){
				alert(data.message);
				return;
			}
			$(window).attr('location', "./addresslist.html");
		}
	})

})

$("#deleteBtn").on('click',function(){
	$.ajax({
		url:perfix + "/address/delete/"+id,
		type:'post',
		headers:{
			'Authorization': localStorage.getItem("token")
		},
		contentType:'application/json',
		success: function(data){
			if (data.code == 10006) {
				localStorage.clear();
				$(window).attr('location', "login.html");
			}
			$(window).attr('location', "addresslist.html");
		}
	})
})
